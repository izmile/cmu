//-------------------------------------------------------------------------------------------------
// This file has the function definitions for tty serial interface
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#ifndef _TTY_SERIAL_H_
#define _TTY_SERIAL_H_

enum TTY_SERIAL_ERROR
{
    TTY_SERIAL_NO_ERROR,
    // All tty errors shall have error codes from 1
    TTY_SERIAL_TTY_OPEN_ERROR,
    TTY_SERIAL_TTY_SETTINGS_ERROR,
    TTY_SERIAL_WRITE_ERROR,
    TTY_SERIAL_READ_ERROR,
    TTY_SERIAL_READ_EOF,
    TTY_SERIAL_PORT_NOT_OPEN,
    TTY_SERIAL_TRANSCEIVE_PARAMS_INCORRECT,
    TTY_SERIAL_UNKNOWN_ERROR,                              // Keep this as the last in the enum list
                                                           // cpm_device.h extends the error codes from this
};

class TTY_SERIAL
{
    private:
        int m_fdesc;
    public:
        TTY_SERIAL();
        virtual ~TTY_SERIAL();
        int Open(char *_tty /*tty_params _params*/);
        int Read(char *_buffer, int _size);
        int Write(char *_buffer, int _size);
        int Transceive(char *_tx_buffer, int _tx_Size, char *_rx_buffer, int *_rx_size, int _write_to_read_wait_mS, int _retries);
        void Close(void);
        bool isOpen(void);
        void Set_Line_Control(int _LineID, bool _State);
};

#endif