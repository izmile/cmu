//-------------------------------------------------------------------------------------------------
// This file has the function definitions for hidraw interface
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#ifndef _HIDRAW_H_
#define _HIDRAW_H_

enum HIDRAW_ERROR
{
    HIDRAW_NO_ERROR,
    // All tty errors shall have error codes from -200 to -101
    HIDRAW_OPEN_ERROR                             = -200,
    HIDRAW_WRITE_ERROR,
    HIDRAW_READ_ERROR,
    HIDRAW_READ_EOF,
    HIDRAW_NOT_OPEN,
    HIDRAW_TRANSCEIVE_PARAMS_INCORRECT,
    HIDRAW_UNKNOWN_ERROR,                                  // Keep this as the last in the enum list
                                                           // cpm_device.h extends the error codes from this
};

class HIDRAW
{
    private:
        int m_fdesc;
    public:
        HIDRAW();
        virtual ~HIDRAW();
        int Open(char *_hidraw_device /*tty_params _params*/);
        int Read(char *_buffer, int _size);
        int Write(char _report_ID, char *_buffer, int _size);
        int Transceive(char _report_ID, char *_tx_buffer, int _tx_Size, char *_rx_buffer, int *_rx_size, int _write_to_read_wait_mS, int _retries);
        void Close(void);
        bool isOpen(void);
};

#endif