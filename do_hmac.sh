#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Inject the HMAC Salt to CPM
#
# > do_hmac.sh <keyset> <-q/-d> <tty device>
#
#--------------------------------------------------------------------------------------------------
KEYSET="${1:-dev}"
OPTION1="${2:--d}"
TTY="${3:-/dev/ttyACM0}"
VERSION="0.2"
EXIT_VAL=1
HMAC_ROOT="./03_tset"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [ $# -eq 0 ]; then
   echo "HMAC salt Injector v"$VERSION
   echo "Usage:"
   echo "do_hmac.sh <keyset> <-q/-d> <tty device (default = /dev/ttyACM0)>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
case $KEYSET in
    dev)
        HMAC_SOURCE="${HMAC_ROOT}/01_dev_ks1"
    ;;
    nfp)
        HMAC_SOURCE="${HMAC_ROOT}/02_nfp_ks"
    ;;
    *)
        echo "ERROR: unsupported keyset!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -tty $TTY $OPTION1 -skin hmac
if [ $? -ne 0 ]; then
   echo "ERROR : Start key injection"
   exit $EXIT_VAL
fi
read -p "Pause.. for 1 secs" -t 1

#--------------------------------------------------------------------------------------------------
cd $HMAC_SOURCE
for file in *; do
    ((EXIT_VAL=EXIT_VAL+1))
    PROCESSED=0
    if [ -f "$file" ]; then
        case $file in
            *server*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wssvr $file
            ;;
            *session.rsa)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wsess $file
            ;;
            *session.rsa.sig)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wsign $file
            ;;
            *tr31*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wtr31hmac $file
            ;;
	    *)
                break
	    ;;
        esac
    fi
    if [ $? -ne 0 ]; then
        echo "ERROR : ${FAILED_FILE} injection"
        exit $EXIT_VAL
    fi
    if (( PROCESSED == 1 )); then
        read -p "Pause.. for 1 secs" -t 1
        echo ""
    fi
done

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
../../$EXECUTABLE -tty $TTY $OPTION1 -ekin
if [ $? -ne 0 ]; then
   echo "ERROR : End key injection"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "HMAC salt injection complete"
exit 0
#--------------------------------------------------------------------------------------------------




