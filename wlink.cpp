//-------------------------------------------------------------------------------------------------
// This file has the function definitions for wlink interface
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h> 
#include <string.h>
#include "wlink.h"
#include "common.h"

uint8_t Get_Descriptor_Cmd[]                      =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xD0, 0x00, 0x01, 0x00,
                                                       0x00, 0x00, 0x03 };
uint8_t File_Write_Init[]                         =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xFE, 0x00, 0x08, 0x01,
                                                       0x03, 0x01, 0x01, 0x00, 0x00, 0x03, 0x0A, 0x00, 0x00, 0x03 };
uint8_t File_Write_Chunk[WLINK_PACKET_SIZE + 10]  =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xFE, 0x00, 0x00, 0x03 };
uint8_t File_Write_End[]                          =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xFE, 0x00, 0x01, 0x04,
                                                       0x00, 0x00, 0x03 };

uint8_t File_Read_Init[]                          =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xFE, 0x00, 0x04, 0x01,
                                                       0x02, 0x01, 0x01, 0x00, 0x00, 0x03 };							   
uint8_t File_Read_Chunk[]                         =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xFE, 0x00, 0x04, 0x02,
                                                       0x02, 0x01, 0x01, 0x00, 0x00, 0x03 };

uint8_t Key_Inject_Init[]                         =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xD1, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x03 };
uint8_t Key_Inject_End[]                          =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0xD1, 0x00, 0x01, 0x01, 0x00, 0x00, 0x03 };

uint8_t RFR[]                                     =  { 0x02, 0x01, 0x0B, 0x01, 0x0E, 0x01, 0x90, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x03};

//uint8_t Scratch_Buffer[256];

WLINK::WLINK()
{
    m_UI_Mode = WLINK_UI_NORMAL_TEXT;
    return;
}

WLINK::~WLINK()
{
    return;
}

//-------------------------------------------------------------------------------------------------
// Setup the debug mode variable
//-------------------------------------------------------------------------------------------------
void WLINK::Set_UI_Mode(int _mode)
{
	m_UI_Mode = (WLINK_UI_MODE)_mode;
	return;
}

//-------------------------------------------------------------------------------------------------
// Returns the Wlink CRC
// BUYPASS CRC
//-------------------------------------------------------------------------------------------------
uint16_t WLINK::Get_CRC16(char *_Buffer, int _Size)
{
	uint16_t _crc = 0;
	char _index;

	while(_Size--)
	{
		_crc = _crc ^ *_Buffer++ << 8;
		for (_index = 0; _index < 8; _index++)
		{
			if (_crc & 0x8000)
			{
				_crc = _crc << 1 ^ 0x8005;
			}
			else
			{
				_crc = _crc << 1;
			}
		}
	}
	return(_crc);
}

//-------------------------------------------------------------------------------------------------
// Transceives a Wlink packet. 
// All transfer parameters shall be loaded in the member variable m_WlinkXferParams
// Returns :   0 -> on success
//           < 0 -> on failure
// *_rx_size will have the number of valid bytes in _rx_buffer
//-------------------------------------------------------------------------------------------------
int WLINK::Tranceive_Packet(char *_tx_buffer, int _tx_size, char *_rx_buffer, int *_rx_size, int _write_to_read_wait_mS, int _retries)
{
	int _retval = WLINK_NO_ERROR;
	uint16_t _crc;
	do
	{
		if(!_tx_buffer || 
		   _tx_size < 10 || 
		   !_rx_buffer || 
		   *_rx_size < 10)
		{
            _retval = WLINK_PARAMS_ERROR;
			break;
		}

		_crc = Get_CRC16(_tx_buffer + 1, _tx_size - 4);    // Calculate the packet CRC
		_tx_buffer[_tx_size - 3] = _crc >> 8;              // Append the CRC
		_tx_buffer[_tx_size - 2] = _crc & 0xFF; 
		_tx_buffer[_tx_size - 1] = 0x03;                   // Append the trailer byte

		switch(m_UI_Mode)
		{
			case(WLINK_UI_JSON):
			case(WLINK_UI_STEALTH):
			{
				break;
			}
			case(WLINK_UI_NORMAL_TEXT):
			{
				printf("#");
				fflush(stdout);
				break;
			}
			case(WLINK_UI_DEBUG):
			{
				printf("\n>> ");
				for(int i = 0; i < _tx_size; i++)
				{
					printf("%02X ", (uint8_t)_tx_buffer[i]);
				}
				fflush(stdout);
				break;
			}
		}

		_retval = Transceive(_tx_buffer, 
		                     _tx_size, 
		                     _rx_buffer, 
		                     _rx_size, 
		                     _write_to_read_wait_mS, 
		                     _retries);

		if(_retval < 0)
		{
			break;
		}

		if(m_UI_Mode == WLINK_UI_DEBUG)
		{
			printf("\n<< ");
			for(int i = 0; i < *_rx_size; i++)
			{
				printf("%02X ", (uint8_t)_rx_buffer[i]);
			}
			fflush(stdout);
		}
		
		// Check received packet size
		if(*_rx_size < 10)
		{
			printf("\nPacket < 10");
			_retval = WLINK_RX_PACKET_TOO_SMALL;
			break;
		}

		// Check the received packet integrity
		_crc = Get_CRC16(_rx_buffer + 1, *_rx_size - 4);
		//printf("\nRX calc CRC = %X", _crc);
		//printf("\nRX crc = %X %X", _rx_buffer[*_rx_size - 3], _rx_buffer[*_rx_size - 2]);
		if((_crc >> 8) !=  (_rx_buffer[*_rx_size - 3] & 0xFF) ||
		   (_crc & 0xFF) != (_rx_buffer[*_rx_size - 2] & 0xFF))
		{
			_retval = WLINK_CRC_ERROR;
			break;
		}

	}while(0);
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transmits a wlink packet - Received response is ignored
// _write_to_read_wait_mS and _retries are required even for transmit only as the lower level
// functions will read the response back from the reader. If the response is not received or 
// incorrect then an appropriate error code will be set
//-------------------------------------------------------------------------------------------------
int WLINK::Transmit_Packet(char *_tx_buffer, int _tx_size, int _write_to_read_wait_mS, int _retries, char *_return_code)
{
	int _retval;
	char *_buffer; 
	int _buffer_size;

	do
	{
		_buffer = (char *)malloc(WLINK_PACKET_SIZE);       // Allocate buffer memory
		if(_buffer == NULL)                                // If memory allocation failed, then set error and quit
		{
			_retval = WLINK_MALLOC_ERROR;
			break;
		}

		_buffer_size = WLINK_PACKET_SIZE;
		// Transceive the wlink packet
        _retval = Tranceive_Packet(_tx_buffer, _tx_size, _buffer, &_buffer_size, _write_to_read_wait_mS, _retries);
		if(_retval < 0)
		{
			break;
		}  
		if(_return_code)
		{
			*_return_code = _buffer[WLINK_RETURN_CODE_INDEX];
		}
	}while(0);

	if(_buffer)                                            // If buffer is valid, then free it
	{
    	free(_buffer);
	}
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Writes a file over Wlink interface
// Returns :   0 -> on success
//           < 0 -> on failure
//-------------------------------------------------------------------------------------------------
int WLINK::Write_File(uint16_t _file_type, char *_file_path, int _extra_tout_mS)
{
    int _retval = WLINK_NO_ERROR;
    FILE *_fp;
    char *_buffer;
	long _file_size, _file_ptr, fread_size;
	int _chunk_size, _buffer_size;
	bool _isError;

    do
    {
		_buffer = (char *)malloc(WLINK_PACKET_SIZE);       // Allocate buffer memory
		if(_buffer == NULL)                                // If memory allocation failed, then set error and quit
		{
			_retval = WLINK_MALLOC_ERROR;
			break;
		}
        _fp = fopen(_file_path, "rb");                     // Open the file to load
        if(!_fp)                                           // If file open error, then quit with an error
        {
            _retval = WLINK_FILE_OPEN_ERROR;
            break;
        }
		fseek(_fp, 0, SEEK_END);                           // Get the file size 
		_file_size = ftell(_fp);
		fseek(_fp, 0, SEEK_SET);

		//printf("\nFilesize = %X", _file_size);
		if(!_file_size)
		{
			_retval = WLINK_EMPTY_FILE_ERROR;
			break;
		}

		//------------------------------------------------------------------------------------------------
		// Initate File opeation
		//------------------------------------------------------------------------------------------------
		File_Write_Init[11] = (_file_type >> 8) & 0xFF;    // Set the file type in the wlink packet
		File_Write_Init[12] = _file_type & 0xFF;
		//printf("\nFile type = %X", _file_type);

		File_Write_Init[13] = (_file_size >> 24) & 0xFF;    // Set the file size in the wlink packet
		File_Write_Init[14] = (_file_size >> 16) & 0xFF;
		File_Write_Init[15] = (_file_size >> 8) & 0xFF;
		File_Write_Init[16] = _file_size & 0xFF;

		_buffer_size = WLINK_PACKET_SIZE;
		// Transceive the file write initalisation wlink packet
        _retval = Tranceive_Packet((char *)File_Write_Init, sizeof(File_Write_Init), _buffer, &_buffer_size, WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 3);
		if(_retval)
		{
			break;
		}
		if(_buffer[WLINK_RETURN_CODE_INDEX])  
		{
			_retval = WLINK_OPERATION_FAILED;
			break;
		}     

		//------------------------------------------------------------------------------------------------
		// Write the file in chunks
		//------------------------------------------------------------------------------------------------
		_file_ptr = 0;                                     // Intialise the file_pointer
		_isError = false;
		while(_file_ptr < _file_size)                      // Loop as long as the file_pointer < file_size
		{
			//usleep(500 * 1000);
			_chunk_size = WLINK_CHUNK_SIZE;
			if((_file_size - _file_ptr) < WLINK_CHUNK_SIZE)
			{
				_chunk_size = (uint16_t)(_file_size - _file_ptr);
			}

			// Read the chunk of data from the file
			fread_size = fread(&File_Write_Chunk[10], sizeof(uint8_t), _chunk_size, _fp);
			if(fread_size < _chunk_size)
			{
				_retval = WLINK_FILE_LOADING_ERROR;
				_isError = true;;
				break;
			}

			// Write the size
			File_Write_Chunk[7] = (uint8_t)((1 + _chunk_size) >> 8);   // 1 is for sub-command
			File_Write_Chunk[8] = (uint8_t)((1 + _chunk_size) & 0xFF); // 1 is for sub-command
			_chunk_size += (10 + 3);

			//printf("\nChunksize = %d", _chunk_size);
			_buffer_size = WLINK_PACKET_SIZE;
			// Transceive the file write chunk
			_retval = Tranceive_Packet((char *)File_Write_Chunk, _chunk_size, _buffer, &_buffer_size, WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 20);
			if(_retval)
			{
				_isError = true;
				break;
			} 
			if(_buffer[WLINK_RETURN_CODE_INDEX])  
			{
				_isError = true;
				_retval = WLINK_OPERATION_FAILED;
				break;
			} 

			_file_ptr = ftell(_fp);
		}

		if(_isError)
		{
			break;
		}

		//------------------------------------------------------------------------------------------------
		// End File operation
		//------------------------------------------------------------------------------------------------
		//usleep(500 * 1000);  
		_buffer_size = WLINK_PACKET_SIZE;
		// Transceive the file end wlink packet
        _retval = Tranceive_Packet((char *)File_Write_End, sizeof(File_Write_End), _buffer, &_buffer_size, WLINK_EXTENDED_WAIT_mS + _extra_tout_mS, 3);
		if(_retval)
		{
			// Ignore the error on end packet
			//_retva = 0;
			break;
		} 
		if(_buffer[WLINK_RETURN_CODE_INDEX])  
		{
			// Ignore the error on end packet
			//_retval = 0;
			_retval = WLINK_OPERATION_FAILED;				
			break;
		} 

    } while(0);
    
	if(_buffer)                                            // If buffer is valid, then free it
	{
    	free(_buffer);
	}
	if(_fp)                                                // If file is open, then close it
	{
		fclose(_fp);
	}
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Reads the given file from CPM module and saves it to a file
// Returns :   0 -> on success
//           < 0 -> on failure
//-------------------------------------------------------------------------------------------------
int WLINK::Read_File(uint16_t _file_type, char *_save_path, int _extra_tout_mS)
{
    int _retval = WLINK_NO_ERROR;
    FILE *_fp;
    char *_rx_buffer = NULL, *_tx_buffer = NULL;
	int _chunk_size, _tx_size, _rx_buffer_size, _rx_data_size, fwrite_size;
	WLINK_PACKET *_wlink_tx_pkt, *_wlink_rx_pkt;
	bool _isError = false;

    do
    {
		_tx_buffer = (char *)malloc(WLINK_PACKET_SIZE);       // Allocate buffer memory
		if(_tx_buffer == NULL)                                // If memory allocation failed, then set error and quit
		{
			_retval = WLINK_MALLOC_ERROR;
			break;
		}
		_rx_buffer = (char *)malloc(WLINK_PACKET_SIZE);       // Allocate buffer memory
		if(_rx_buffer == NULL)                                // If memory allocation failed, then set error and quit
		{
			_retval = WLINK_MALLOC_ERROR;
			break;
		}
        _fp = fopen(_save_path, "wb");                       // Open the file to save
        if(!_fp)                                              // If file open error, then quit with an error
        {
            _retval = WLINK_FILE_OPEN_ERROR;
            break;
        }
		_wlink_tx_pkt = (WLINK_PACKET *) _tx_buffer;
		_wlink_rx_pkt = (WLINK_PACKET *) _rx_buffer;          // Assign the Wlink packet pointer

		//------------------------------------------------------------------------------------------------
		// Initate File opeation
		//------------------------------------------------------------------------------------------------
		_wlink_tx_pkt->STX = WLINK_STX;
		_wlink_tx_pkt->Sequence = 0x01;
		_wlink_tx_pkt->Sender_ID = WLINK_TERMINAL_ID;
		_wlink_tx_pkt->Receiver_ID = WLINK_READER_ID;
		_wlink_tx_pkt->Instruction = WLINK_INST_FILE_MGMT;
		_wlink_tx_pkt->Data_Size = uint_16_Toggle_Endian(4);
		_wlink_tx_pkt->Data[0] = WLINK_SCMD_FILE_INIT;
		_wlink_tx_pkt->Data[1] = WLINK_FILE_READ_MODE;
		_wlink_tx_pkt->Data[2] = (_file_type >> 8) & 0xFF;
		_wlink_tx_pkt->Data[3] = _file_type & 0xFF;
		_tx_size = ((char *)&_wlink_tx_pkt->Data[3] - (char *)&_wlink_tx_pkt->STX) + 4;
		_rx_buffer_size = WLINK_PACKET_SIZE;
		printf("\nPkt size = %d", _tx_size);
		// Transceive the file read initalisation wlink packet
        _retval = Tranceive_Packet((char *)_wlink_tx_pkt, _tx_size, (char *)_wlink_rx_pkt, &_rx_buffer_size, WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 3);
		if(_retval)
		{
			break;
		}
		if(uint_16_Toggle_Endian(_wlink_rx_pkt->Data_Size) < 5 || _wlink_rx_pkt->Data[0])  
		{
			_retval = WLINK_OPERATION_FAILED;
			break;
		}     
		_chunk_size = *((int *)&_wlink_rx_pkt->Data[1]);
		_chunk_size = uint_32_Toggle_Endian(_chunk_size);
		printf("\nRead chunksize = %X", _chunk_size);

		//------------------------------------------------------------------------------------------------
		// Save the file in chunks
		//------------------------------------------------------------------------------------------------
		while(_chunk_size)
		{
			_wlink_tx_pkt->STX = WLINK_STX;
			_wlink_tx_pkt->Sequence = 0x01;
			_wlink_tx_pkt->Sender_ID = WLINK_TERMINAL_ID;
			_wlink_tx_pkt->Receiver_ID = WLINK_READER_ID;
			_wlink_tx_pkt->Instruction = WLINK_INST_FILE_MGMT;
			_wlink_tx_pkt->Data_Size = uint_16_Toggle_Endian(1);
			_wlink_tx_pkt->Data[0] = WLINK_SCMD_FILE_READ;
			_tx_size = ((char *)&_wlink_tx_pkt->Data[0] - (char *)&_wlink_tx_pkt->STX) + 4;
			_rx_buffer_size = WLINK_PACKET_SIZE;
			// Transceive the file read chunk wlink packet
        	_retval = Tranceive_Packet((char *)_wlink_tx_pkt, _tx_size, (char *)_wlink_rx_pkt, &_rx_buffer_size, WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 3);
			if(_retval)
			{
				_isError = true;
				break;
			}			
			if(_wlink_rx_pkt->Data[0])  
			{
				_retval = WLINK_OPERATION_FAILED;
				_isError = true;
				break;
			}
			_rx_data_size = uint_16_Toggle_Endian(_wlink_rx_pkt->Data_Size) - 1; // -1 for return code
			if(_rx_data_size <= 0)
			{
				_retval = WLINK_READ_DATA_ERROR;
				_isError = true;
				break;
			}
			// Write the chunk of data to the file
			fwrite_size = fwrite(&_wlink_rx_pkt->Data[1], sizeof(uint8_t), _rx_data_size, _fp);
			if(fwrite_size < _rx_data_size)
			{
				_retval = WLINK_FILE_WRITE_ERROR;
				_isError = true;
				break;
			}
			_chunk_size -= _rx_data_size;
		}

		if(_isError)
		{
			break;
		}

		//------------------------------------------------------------------------------------------------
		// End File operation
		//------------------------------------------------------------------------------------------------
		_wlink_tx_pkt->STX = WLINK_STX;
		_wlink_tx_pkt->Sequence = 0x01;
		_wlink_tx_pkt->Sender_ID = WLINK_TERMINAL_ID;
		_wlink_tx_pkt->Receiver_ID = WLINK_READER_ID;
		_wlink_tx_pkt->Instruction = WLINK_INST_FILE_MGMT;
		_wlink_tx_pkt->Data_Size = uint_16_Toggle_Endian(1);
		_wlink_tx_pkt->Data[0] = WLINK_SCMD_FILE_END;
		_tx_size = ((char *)&_wlink_tx_pkt->Data[0] - (char *)&_wlink_tx_pkt->STX) + 4;
		_rx_buffer_size = WLINK_PACKET_SIZE;
		// Transceive the file end wlink packet
        _retval = Tranceive_Packet((char *)_wlink_tx_pkt, _tx_size, (char *)_wlink_rx_pkt, &_rx_buffer_size, WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 3);
		if(_retval)
		{
			break;
		}
		if(_wlink_rx_pkt->Data[0])  
		{
			_retval = WLINK_OPERATION_FAILED;
			break;
		}  
    } while(0);
    
	if(_tx_buffer)                                         // If buffer is valid, then free it
	{
		free(_tx_buffer);
	}
	if(_rx_buffer)                                         // If buffer is valid, then free it
	{
    	free(_rx_buffer);
	}
	if(_fp)                                                // If file is open, then close it
	{
		fclose(_fp);
	}
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives Get_Discriptor WLINK command
//-------------------------------------------------------------------------------------------------
int WLINK::Get_Descriptor(int _extra_tout_mS)
{
	int _retval;
	char _return_code;
	_retval = Transmit_Packet((char *)Get_Descriptor_Cmd, 
	                          sizeof(Get_Descriptor_Cmd), 
							  WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 
							  WLINK_RESPONSE_RETRIES, 
							  &_return_code);
	if(!_retval)
	{
		if(_return_code)
		{
			_retval = WLINK_OPERATION_FAILED;
		}
	}
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives Key_Inject_Init WLINK command
//-------------------------------------------------------------------------------------------------
int WLINK::Key_Injection_Init(KEY_INJECTION_TYPE _key_type, int _extra_tout_mS)
{
	int _retval;
	char _return_code;
	Key_Inject_Init[sizeof(Key_Inject_Init) - 4] = (char)(_key_type & 0xFF);
	_retval = Transmit_Packet((char *)Key_Inject_Init, 
	                          sizeof(Key_Inject_Init), 
							  WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 
							  WLINK_RESPONSE_RETRIES, 
							  &_return_code);
	if(!_retval)
	{
		if(_return_code)
		{
			_retval = WLINK_OPERATION_FAILED;
		}
	}	
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives Key_Inject_End WLINK command
//-------------------------------------------------------------------------------------------------
int WLINK::Key_Injection_End(int _extra_tout_mS)
{
	int _retval;
	char _return_code;
	_retval = Transmit_Packet((char *)Key_Inject_End, 
	                          sizeof(Key_Inject_End), 
							  WLINK_RESPONSE_WAIT_mS + _extra_tout_mS, 
							  WLINK_EXTENDED_RESPONSE_RETRIES, 
							  &_return_code);
	if(!_retval)
	{
		if(_return_code)
		{
			_retval = WLINK_OPERATION_FAILED;
		}
	}
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives Request-for-Request WLINK command
// _RFR_type value       Description
// 01                    RFR for RKI SIGN_CONF_MAIN to be loaded
// 02                    RFR for RKI DUKPT_INIT_PAY to be loaded
// 03                    RFR for Terminal settings to be loaded
// 04                    RFR for SBL to be loaded
// 05                    RFR for FW to be loaded
//-------------------------------------------------------------------------------------------------
int WLINK::RFR_Command(uint8_t _RFR_type, int _extra_tout_mS)
{
	int _retval;
	char _return_code;
	RFR[10] = _RFR_type;
	_retval = Transmit_Packet((char *)RFR, 
	                          sizeof(RFR), 
							  WLINK_RESPONSE_WAIT_mS  + _extra_tout_mS, 
							  WLINK_EXTENDED_RESPONSE_RETRIES, 
							  &_return_code);
	if(!_retval)
	{
		if(_return_code)
		{
			_retval = WLINK_OPERATION_FAILED;
		}
	}
	return(_retval);
}
