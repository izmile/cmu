//-------------------------------------------------------------------------------------------------
// This file has the function declerations for wlink interface
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#ifndef _WLINK_H_
#define _WLINK_H_

#include "tty_serial.h"

#define WLINK_PACKET_SIZE                                  1024
#define WLINK_CHUNK_SIZE                                   (WLINK_PACKET_SIZE - 14)
#define WLINK_RETURN_CODE_INDEX                            9
#define WLINK_STX                                          0x02
#define WLINK_TERMINAL_ID                                  0x010B
#define WLINK_READER_ID                                    0x010E
#define WLINK_RESPONSE_WAIT_mS                             200
#define WLINK_RESPONSE_RETRIES                             3
#define WLINK_EXTENDED_RESPONSE_RETRIES                    (WLINK_RESPONSE_RETRIES)
#define WLINK_EXTENDED_WAIT_mS                             1500

#define WLINK_INST_FILE_MGMT                               0xFE
#define WLINK_FILE_READ_MODE                               0x02
#define WLINK_FILE_WRITE_MODE                              0x03
#define WLINK_SCMD_FILE_INIT                               0x01
#define WLINK_SCMD_FILE_READ                               0x02
#define WLINK_SCMD_FILE_WRITE                              0x03    
#define WLINK_SCMD_FILE_END                                0x04              

#define WLINK_KERNEL_ID_WAVE2                              1
#define WLINK_KERNEL_ID_MCL                                2
#define WLINK_KERNEL_ID_VISA                               3
#define WLINK_KERNEL_ID_AMEX                               4
#define WLINK_KERNEL_ID_JCB                                5
#define WLINK_KERNEL_ID_DPASS                              6
#define WLINK_KERNEL_ID_CUP                                7
#define WLINK_KERNEL_ID_EMVCT                              8
#define WLINK_KERNEL_ID_MSR                                9
#define WLINK_KERNEL_ID_INTERAC                            10
#define WLINK_KERNEL_ID_PURE                               11
#define WLINK_KERNEL_ID_EFTPOS                             12
#define WLINK_KERNEL_ID_GIROCARD                           0x2A
#define WLINK_KERNEL_ID_TOTAL                              0x2B
#define WLINK_KERNEL_ID_UNKNOWN                            0xFF

#define WLINK_FILE_ID_EMV_CA                               0x0101
#define WLINK_FILE_ID_LANGUAGE                             0x0102
#define WLINK_FILE_ID_REVOCATION                           0x0103
#define WLINK_FILE_ID_EXCEPTION                            0x0104
#define WLINK_FILE_ID_SBL                                  0x0201
#define WLINK_FILE_ID_FW                                   0x0202
#define WLINK_FILE_ID_RSA_SIGN_MANUF                       0x0300
#define WLINK_FILE_ID_RSA_ENC_TERM                         0x0301
#define WLINK_FILE_ID_SERIAL_NUMBER                        0x0302
#define WLINK_FILE_ID_RSA_SIGN_TERM                        0x0303
#define WLINK_FILE_ID_RSA_SIGN_SERVER                      0x0310
#define WLINK_FILE_ID_KI_SESSION                           0x0320
#define WLINK_FILE_ID_KI_SESSION_SIGN                      0x0321
#define WLINK_FILE_ID_KI_DUKPT_PAY                         0x0330
#define WLINK_FILE_ID_KI_DUKPT_PAY_KSN                     0x0340
#define WLINK_FILE_ID_RSA_SIGN_CONF_MAIN                   0x0350
#define WLINT_FILE_ID_KI_HMAC_SALT                         0x0360
#define WLINK_FILE_ID_BIN_LIST                             0x0401
#define WLINK_FILE_PAYWAVE_SETTINGS                        (0x0000 | WLINK_KERNEL_ID_VISA)
#define WLINK_FILE_MCL_SETTINGS                            (0x0000 | WLINK_KERNEL_ID_MCL)
#define WLINK_FILE_AXP_SETTINGS                            (0x0000 | WLINK_KERNEL_ID_AMEX)
#define WLINK_FILE_DPASS_SETTINGS                          (0x0000 | WLINK_KERNEL_ID_DPASS)
#define WLINK_FILE_JCB_SETTINGS                            (0x0000 | WLINK_KERNEL_ID_JCB)
#define WLINK_FILE_CUP_SETTINGS                            (0x0000 | WLINK_KERNEL_ID_CUP)

enum WLINK_ERROR
{
    WLINK_NO_ERROR,
    WLINK_PARAMS_ERROR                                     = TTY_SERIAL_UNKNOWN_ERROR + 1,  // Extend the error code from TTY_SERIAL_UNKNOWN_ERROR   
    WLINK_FILE_OPEN_ERROR,
    WLINK_MALLOC_ERROR,
    WLINK_FILE_LOADING_ERROR,
    WLINK_CRC_ERROR,
    WLINK_RX_PACKET_TOO_SMALL,
    WLINK_OPERATION_FAILED,
    WLINK_EMPTY_FILE_ERROR,
    WLINK_READ_DATA_ERROR,
    WLINK_FILE_WRITE_ERROR,
    WLINK_UNKNOWN_ERROR,
};

enum WLINK_UI_MODE
{
    WLINK_UI_STEALTH,
    WLINK_UI_JSON,
    WLINK_UI_NORMAL_TEXT,
    WLINK_UI_DEBUG,
};

enum KEY_INJECTION_TYPE
{
    WLINK_KEY_INJECTION_DUKPT = 0x01,
    WLINK_KEY_INJECTION_SIGN_CONF,
    WLINK_KEY_INJECTION_HMAC_SALT,
};

#pragma pack(1)
struct WLINK_PACKET
{
    uint8_t  STX;
    uint8_t Sequence;
    uint16_t Sender_ID;
    uint16_t Receiver_ID;
    uint8_t Instruction;
    uint16_t Data_Size;
    uint8_t Data[256];
};

class WLINK : public TTY_SERIAL
{
    private:
        WLINK_UI_MODE  m_UI_Mode;
    public:
        WLINK();
        virtual ~WLINK();
        void Set_UI_Mode(int _mode);
        uint16_t Get_CRC16(char *_Buffer, int _Size);
        int Tranceive_Packet(char *_tx_buffer, int _tx_size, char *_rx_buffer, int *_rx_size, int _write_to_read_wait_mS, int _retries);
        int Transmit_Packet(char *_tx_buffer, int _tx_size, int _write_to_read_wait_mS, int _retries, char *_return_code);
        int Write_File(uint16_t _file_type, char *_file_path, int _extra_tout_mS);
        int Read_File(uint16_t _file_type, char *_save_path, int _extra_tout_mS);
        int Get_Descriptor(int _extra_tout_mS);
        int Key_Injection_Init(KEY_INJECTION_TYPE _key_type, int _extra_tout_mS);
        int Key_Injection_End(int _extra_tout_mS);
        int RFR_Command(uint8_t _RFR_type, int _extra_tout_mS);
        //int Inject_Key(uint8_t _key_type, char *_key_path);
};

#endif
