//-------------------------------------------------------------------------------------------------
// This file has the function definitions for HID communication 
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-June-2020
//-------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>                                         /* File Control Definitions           */
#include <termios.h>                                       /* POSIX Terminal Control Definitions */
#include <unistd.h>                                        /* UNIX Standard Definitions 	     */ 
#include <errno.h> 
#include <sys/ioctl.h>

#include "hidraw.h"

HIDRAW::HIDRAW()
{
    m_fdesc = -1;
    //printf("\nhidraw construct");
    return;
}

HIDRAW::~HIDRAW()
{
    //printf("\nhidraaw destruct");
    return;
}

//-------------------------------------------------------------------------------------------------
// Open a given hidraw device
//-------------------------------------------------------------------------------------------------
int HIDRAW::Open(char *_hidraw_device /*hidraw_params _params*/)
{
    int _retval = HIDRAW_NO_ERROR;
    do
    {
        //printf("\n%s", _hidraw);
        m_fdesc = open(_hidraw_device,O_RDWR | O_NDELAY); 
			   						                       /* O_RDWR Read/Write access to serial HIDRAW           */
									                       /* O_NDELAY -Non Blocking Mode,Does not care about-  */
									                       /* -the status of DCD line,Open() returns immediatly */                                        

        if(m_fdesc == -1)
        {
            _retval = HIDRAW_OPEN_ERROR;
            //printf("\nopen error");
            break;
        }
    } while (0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Reads data from hidraw device that is already open
// Returns the number of bytes read
// 0 = HIDRAW_READ_EOF
// -1 = Read error
// HIDRAW_NOT_OPEN -ve value = device not open
//
// NOTE: This function uses the HID Read method through EP11. Not Get_Report over EP0
//-------------------------------------------------------------------------------------------------
int HIDRAW::Read(char *_buffer, int _size)
{
    int _retval;
    if(isOpen())                                           // Check if the hidraw device is open
    {
        //printf("\nRead size = %u", _size);
        _retval = read(m_fdesc, _buffer, _size);           // Read data from hidraw device
        if(!_retval)                                       // If no data read, the we reached EOF condition
        {
            _retval = HIDRAW_READ_EOF;                     // Set EOF error
        }
        else if(_retval < 0)                               // If read failed, then return an error
        {                                                  
            _retval = HIDRAW_READ_ERROR;     
        }
    }
    else                                                   // hidraw device is not open return error
    {
        _retval = HIDRAW_NOT_OPEN;
    }    
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Writes data into a hidraw device that is already open
// Returns the number of bytes written
// 0 = No bytes written
// -1 = Write error
// HIDRAW_NOT_OPEN -ve value = hidraw not open
//
// NOTE: This function uses the HID Write method through EP11. Not Get_Report over EP0
//-------------------------------------------------------------------------------------------------
int HIDRAW::Write(char _report_ID, char *_buffer, int _size)
{
    int _retval;
    if(isOpen())                                           // Check if the hidraw device is open
    {
#if 0
        _retval = write(m_fdesc, &_report_ID, sizeof(_report_ID)); // Write reportID into the hidraw device
        if(_retval <= 0)                                   // Check if Write has failed, if so return error
        {
            _retval = HIDRAW_WRITE_ERROR;
        }
#endif
        //printf("\nWrite size = %u", _size);
        _retval = write(m_fdesc, _buffer, _size);          // Write data into the hidraw device
        if(_retval <= 0)                                   // Check if Write has failed, if so return error
        {
            _retval = HIDRAW_WRITE_ERROR;
        }
    }
    else                                                   // hidraw device is not open, return an error
    {
        _retval = HIDRAW_NOT_OPEN;
    }    
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives the data over hidraw device
//-------------------------------------------------------------------------------------------------
int HIDRAW::Transceive(char _report_ID, char *_tx_buffer, int _tx_size, char *_rx_buffer, int *_rx_size, 
                           int _write_to_read_wait_mS, int _retries)
{
	int _retval = HIDRAW_NO_ERROR;
    int _status;
	do
	{
        if(_rx_buffer == NULL || *_rx_size <= 0)           // Check if the input parameters are valid
        {                                                  // If so, return error
            _retval = HIDRAW_TRANSCEIVE_PARAMS_INCORRECT;
            break;
        }

        if(_retries < 0)                                   // Check if the number of retries < 0
        { 
            _retries = 0;                                  // if so, set the retries as 0
        }
        
        _status = Write(_report_ID, _tx_buffer, _tx_size);             // Write the serial data
        //printf("\nWrite = %d", _status);
		if(_status <= 0)                                   // Check if any error
		{
            _retval = _status;                             // If so, set the return value as the status
			break;
		}

		do
		{
            //printf("\nwait");
			usleep(_write_to_read_wait_mS * 1000);         // Wait for some time to get the response
            //printf("\nRead");
			_status = Read(_rx_buffer, *_rx_size);         // Read from the hidraw device
            //printf("\nstatus = %d", _status);
			if(_status <= 0)                               // If on read error or no bytes read
			{
                _retval = _status;                         // Set the return value same as status
			}
            else                                           // If read is successful, then
            {
                *_rx_size = _status;                       // Set the number of bytes read
                _retval = HIDRAW_NO_ERROR;
            }
            
		}while(_status <= 0 && _retries--);                // Loop until retries left

	}while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Closes the hidraw device
//-------------------------------------------------------------------------------------------------
void HIDRAW::Close()
{
    if(m_fdesc != -1)
    {
        close(m_fdesc);
        m_fdesc = -1;
    }
    return;
}

//-------------------------------------------------------------------------------------------------
// Returns true if the hidraw device is open, else returns false
//-------------------------------------------------------------------------------------------------
bool HIDRAW::isOpen()
{
    bool _retval = true;
    if(m_fdesc == -1)                                      // Check if the file descriptor is valid
    {
        _retval = false;                                   // If not, return false
    }
    return(_retval);
}

