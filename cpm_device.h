//-------------------------------------------------------------------------------------------------
// This file has the function declerations for CPM_DEVICE class
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#ifndef _CPM_DEVICE_H_
#define _CPM_DEVICE_H_

#include "wlink.h"
#include "hidraw.h"

enum CPM_DEVICE_ERROR
{
    CPM_DEVICE_ERROR,
    // All cmu app errors shall have error codes from -500 to -1
    CPM_DEVICE_FILE_OPEN_ERROR                          = WLINK_UNKNOWN_ERROR + 1,
    CPM_DEVICE_UNKNOWN_ERROR,
};

class CPM_DEVICE //: public WLINK
{
    public:
        WLINK Wlink;
        HIDRAW HIDraw;
        CPM_DEVICE();
        virtual ~CPM_DEVICE();
};

#endif
