#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Inject the SIGN_CONFIG key to CPM
#
# > do_sconf.sh <key set> <-q/-d> <tty device>
#
#--------------------------------------------------------------------------------------------------
KEYSET="${1:-dev}"
OPTION1="${2:--d}"
TTY="${3:-/dev/ttyACM0}"
VERSION="0.2"
EXIT_VAL=1
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [ $# -eq 0 ]; then
   echo "SCONFIG Injector v"$VERSION
   echo "Usage:"
   echo "do_sconf.sh <key set> <-q/-d> <tty device>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
case $KEYSET in
    dev)
        SIGN_CONFIG="./01_sconf/01_dev_ks1/sign_config.der"
    ;;
    nfp)
        SIGN_CONFIG="./01_sconf/02_nfp_ks/sign_config.der"
    ;;
    *)
        echo "ERROR: unsupported keyset!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -tty $TTY $OPTION1 -skin sconf
if [ $? -ne 0 ]; then
   echo "ERROR : Start key injection"
   exit $EXIT_VAL
fi
read -p "Pause.. for 1 secs" -t 1

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -tty $TTY $OPTION1 -wsconf $SIGN_CONFIG
if [ $? -ne 0 ]; then
   echo "Error : ${SIGN_CONFIG} injection"
   exit $EXIT_VAL
fi
read -p "Pause.. for 1 secs" -t 1

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -tty $TTY $OPTION1 -ekin
if [ $? -ne 0 ]; then
   echo "ERROR : End key injection"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "SCONFIG injection complete"
exit 0
#--------------------------------------------------------------------------------------------------
