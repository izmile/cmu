//-------------------------------------------------------------------------------------------------
// This file has the function definitions for serial communication over tty device
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>                                         /* File Control Definitions           */
#include <termios.h>                                       /* POSIX Terminal Control Definitions */
#include <unistd.h>                                        /* UNIX Standard Definitions 	     */ 
#include <errno.h> 
#include <sys/ioctl.h>

#include "tty_serial.h"

TTY_SERIAL::TTY_SERIAL()
{
    m_fdesc = -1;
    //printf("\nTTY construct");
    return;
}

TTY_SERIAL::~TTY_SERIAL()
{
    //printf("\nTTY destruct");
    return;
}

//-------------------------------------------------------------------------------------------------
// Open a given tty port
//-------------------------------------------------------------------------------------------------
int TTY_SERIAL::Open(char *_tty /*tty_params _params*/)
{
    int _retval = TTY_SERIAL_NO_ERROR;
    do
    {
        //printf("\n%s", _tty);
        m_fdesc = open(_tty,O_RDWR | O_NOCTTY | O_NDELAY); /* ttyUSB0 is the FT232 based USB2SERIAL Converter   */
			   						                       /* O_RDWR Read/Write access to serial port           */
									                       /* O_NOCTTY - No terminal will control the process   */
									                       /* O_NDELAY -Non Blocking Mode,Does not care about-  */
									                       /* -the status of DCD line,Open() returns immediatly */                                        

        if(m_fdesc == -1)
        {
            _retval = TTY_SERIAL_TTY_OPEN_ERROR;
            //printf("\nopen error");
            break;
        }
		struct termios SerialPortSettings;	               /* Create the structure                          */

		//tcgetattr(m_fdesc, &SerialPortSettings);	       /* Get the current attributes of the Serial port */
		cfsetispeed(&SerialPortSettings, B115200);         /* Set Read  Speed as 115200                       */
		cfsetospeed(&SerialPortSettings, B115200);         /* Set Write Speed as 115200                       */
        cfmakeraw(&SerialPortSettings);

		SerialPortSettings.c_cflag &= ~PARENB;             /* Disables the Parity Enable bit(PARENB),So No Parity   */
		SerialPortSettings.c_cflag &= ~CSTOPB;             /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
		SerialPortSettings.c_cflag &= ~CSIZE;	           /* Clears the mask for setting the data size             */
		SerialPortSettings.c_cflag |=  CS8;                /* Set the data bits = 8                                 */
		SerialPortSettings.c_cflag &= ~CRTSCTS;            /* No Hardware flow Control                         */
		SerialPortSettings.c_cflag |= CREAD | CLOCAL;      /* Enable receiver,Ignore Modem Control lines       */ 		
		SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
		SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */
		SerialPortSettings.c_oflag &= ~OPOST;              /*No Output Processing*/
        SerialPortSettings.c_cc[VMIN] = 1;                 /* Min 1 bytes is good to return for read() */
        SerialPortSettings.c_cc[VTIME] = 5;                /* Inter-character delay */

        //sleep(2); //required to make flush work, for some reason
        tcflush(m_fdesc, TCIOFLUSH);

		if((tcsetattr(m_fdesc, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        {
            _retval = TTY_SERIAL_TTY_SETTINGS_ERROR;
            //printf("\nsetting error");
            break;
        }

        Set_Line_Control(TIOCM_DTR, true);                 // Set DTR high. CPM firmware knows the connection status through this signal

    } while (0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Reads data from a tty port that is already open
// Returns the number of bytes read
// 0 = EOF
// -1 = Read error
// TTY_SERIAL_PORT_NOT_OPEN -ve value = port not open
//-------------------------------------------------------------------------------------------------
int TTY_SERIAL::Read(char *_buffer, int _size)
{
    int _retval;
    if(isOpen())                                           // Check if the tty device is open
    {
        _retval = read(m_fdesc, _buffer, _size);           // Read data from tty device
        if(!_retval)                                       // If no data read, the we reached EOF condition
        {
            _retval = TTY_SERIAL_READ_EOF;                 // Set EOF error
        }
        else if(_retval < 0)                               // If read failed, then return an error
        {                                                  
            _retval = TTY_SERIAL_READ_ERROR;     
        }
    }
    else                                                   // tty device is not open return error
    {
        _retval = TTY_SERIAL_PORT_NOT_OPEN;
    }    
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Writes data into a tty port that is already open
// Returns the number of bytes written
// 0 = No bytes written
// -1 = Write error
// TTY_SERIAL_PORT_NOT_OPEN -ve value = port not open
//-------------------------------------------------------------------------------------------------
int TTY_SERIAL::Write(char *_buffer, int _size)
{
    int _retval;
    if(isOpen())                                           // Check if the tty device is open
    {
        _retval = write(m_fdesc, _buffer, _size);          // Write data into the tty device
        tcdrain(m_fdesc);                                  // Drain the data
        if(_retval <= 0)                                   // Check if Write has failed, if so return error
        {
            _retval = TTY_SERIAL_WRITE_ERROR;
        }
    }
    else                                                   // tty device is not open, return an error
    {
        _retval = TTY_SERIAL_PORT_NOT_OPEN;
    }    
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Transceives the data over tty port
//-------------------------------------------------------------------------------------------------
int TTY_SERIAL::Transceive(char *_tx_buffer, int _tx_size, char *_rx_buffer, int *_rx_size, 
                           int _write_to_read_wait_mS, int _retries)
{
	int _retval = TTY_SERIAL_NO_ERROR;
    int _status;
	do
	{
        if(_rx_buffer == NULL || *_rx_size <= 0)           // Check if the input parameters are valid
        {                                                  // If so, return error
            _retval = TTY_SERIAL_TRANSCEIVE_PARAMS_INCORRECT;
            break;
        }

        if(_retries < 0)                                   // Check if the number of retries < 0
        { 
            _retries = 0;                                  // if so, set the retries as 0
        }
        
        _status = Write(_tx_buffer, _tx_size);             // Write the serial data
        //printf("\nWrite = %d", _status);
		if(_status <= 0)                                   // Check if any error
		{
            _retval = _status;                             // If so, set the return value as the status
			break;
		}

        //printf("\nretries = %d", _retries);
		// Flush RX buffer
		//sleep(2); //required to make flush work, for some reason
        //tcflush(m_fdesc, TCIOFLUSH);
		do
		{
            //printf("\nwait");
			usleep(_write_to_read_wait_mS * 1000);         // Wait for some time to get the response
            //tcflush(m_fdesc, TCIOFLUSH);
            //printf("\nRead");
			_status = Read(_rx_buffer, *_rx_size);         // Read from the tty device
            //printf("\nstatus = %d", _status);
			if(_status <= 0)                               // If on read error or no bytes read
			{
                _retval = _status;                         // Set the return value same as status
			}
            else                                           // If read is successful, then
            {
                *_rx_size = _status;                       // Set the number of bytes read
                _retval = TTY_SERIAL_NO_ERROR;
            }
            
		}while(_status <= 0 && _retries--);                // Loop until retries left

	}while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Closes the tty port
//-------------------------------------------------------------------------------------------------
void TTY_SERIAL::Close()
{
    if(m_fdesc != -1)
    {
        tcflush(m_fdesc, TCIOFLUSH);
        Set_Line_Control(TIOCM_DTR, false);                // Set DTR low
        close(m_fdesc);
        m_fdesc = -1;
    }
    return;
}

//-------------------------------------------------------------------------------------------------
// Returns true if the port is open, else returns false
//-------------------------------------------------------------------------------------------------
bool TTY_SERIAL::isOpen()
{
    bool _retval = true;
    if(m_fdesc == -1)                                      // Check if the file descriptor is valid
    {
        _retval = false;                                   // If not, return false
    }
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Sets the state of RTS/DTR
// _LineID => 
// modem lines - As defined in ioctl-types.h
// #define TIOCM_LE	    0x001
// #define TIOCM_DTR	0x002
// #define TIOCM_RTS	0x004
// #define TIOCM_ST	    0x008
// #define TIOCM_SR	    0x010
// #define TIOCM_CTS	0x020
// #define TIOCM_CAR	0x040
// #define TIOCM_RNG	0x080
// #define TIOCM_DSR	0x100
// #define TIOCM_CD	TIOCM_CAR
// #define TIOCM_RI	TIOCM_RNG
//-------------------------------------------------------------------------------------------------
void TTY_SERIAL::Set_Line_Control(int _LineID, bool _State)
{
    int _Pin;
    _Pin = _LineID;
    if(_State)
        ioctl(m_fdesc, TIOCMBIS, &_Pin); 
    else
        ioctl(m_fdesc, TIOCMBIC, &_Pin); 
    return;
}
