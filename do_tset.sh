#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Inject the terminal settings to CPM
#
# > do_tset.sh <key set> <<-q/-d> <tty device>
#
#--------------------------------------------------------------------------------------------------
KEYSET="${1:-dev}"
OPTION1="${2:--d}"
TTY="${3:-/dev/ttyACM0}"
VERSION="0.2"
EXIT_VAL=1
TSET_ROOT="./03_tset"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [ $# -eq 0 ]; then
   echo "Terminal settings injector v"$VERSION
   echo "Usage:"
   echo "do_tset.sh <key set> <-q/-d> <tty device>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
case $KEYSET in
    dev)
        TSET_SOURCE="${TSET_ROOT}/01_dev_ks1"
    ;;
    nfp)
        TSET_SOURCE="${TSET_ROOT}/02_nfp_ks"
    ;;
    *)
        echo "ERROR: unsupported keyset!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
cd $TSET_SOURCE
for file in *; do
    ((EXIT_VAL=EXIT_VAL+1))
    PROCESSED=0
    if [ -f "$file" ]; then
        case $file in
            *cakeys*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -ucak $file
                #echo $file
            ;;
            *mcl*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -umcl $file
                #echo $file
            ;;
            *visa*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -uvis $file
                #echo $file
            ;;
            *axp*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -uamx $file
                #echo $file
            ;;
            dpas*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -udps $file
                #echo $file
            ;;
            jcb*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -ujcb $file
                #echo $file
            ;;
            except*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -uexc $file
                #echo $file
            ;;
            revoc*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -urev $file
                #echo $file
            ;;
            lang*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -xtout 1000 -ulan $file
                #echo $file
            ;;
        esac
        if [ $? -ne 0 ]; then
            echo "ERROR : ${FAILED_FILE} injection"
            exit $EXIT_VAL
        fi
        if (( PROCESSED == 1 )); then
            read -p "Pause.. for 1 secs" -t 1
            echo ""
        fi
    fi
done

#--------------------------------------------------------------------------------------------------
echo "Terminal settings injection complete"
exit 0
#--------------------------------------------------------------------------------------------------
