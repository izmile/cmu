HMAC Salt: 12345678901234567890123456789012
KBPK: 12345678901234567890123456789012

TR31 block generated with the following header:
"""""""""""""""""""""""""""""""""""""""""""""""
Keyblock version: B
Key usage: M7 (HMAC)
Algorithm: T (TDES / Triple DES)
Mode of use: G (Generate only)
Exportability: N (Non-exportable)
No optional fields are added and padding is all zeroes.


Generated using TR31_Gen.py v0.0
"""""""""""""""""""""""""""""""""
1, Use TR31_Gen.py to generate the TR31 key block
2, Enter the header information as shown below
	Keyblock version: B
	Key usage: M7 (HMAC)
	Algorithm: T (TDES / Triple DES)
	Mode of use: G (Generate only)
	Exportability: N (Non-exportable)
	No optional fields are added and padding is all zeroes.
3, Generate the TR31 block
4, Save the TR31 block in a text file
5, Now, prepare a binary file of the KBPK key with 4 byte header -> 00 00 80 00 <KBPK>

	# Create .rsa wrapped KBPK file using RSA_ENC_TERM public key
	Encrypt KBPK to .rsa file. Note that the KBPK should be a 20 byte TDES key bin file
	First 2 bytes = 00 00 (Indicates that the data is a TDES key)
	Next 2 bytes = 00 80 (128 bits = 16 bytes of TDES key. The first 8 bytes key is reused for TDES)
	Next 16 bytes = TDES key
	Note that the KBPK file is encrypted using RSA_ENC_TERM "public" key. So, the -pubin parameter is required
	openssl pkeyutl -encrypt -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:sha256 -pubin -inkey RSA_ENC_TERM_public.key -in kbpk.bin -out TDES_RKI_SESSION.rsa 

6, RSA encryupt the KBPK.bin file using the reader's enc_term public key. This file is the session.rsa
7, The sign the session.rsa using the sign_server private key. This file is the session.rsa.sig

	# Create .sig for the .rsa file using RSA_SERVER private key
	openssl dgst -sha256 -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -sign RSA_SERVER_private.pem -out test.txt.sig test.txt

8, Now, you should have all the files required for DUKPT injection
