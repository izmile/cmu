BDK: AABBCCDDEEFF11223344556677889933
KSN: FFFF34ABCDEF12000000
IPEK: 2FD76C800E2C0FCDEAD887AA3BB5FD93
KBPK: AC3030FCC988548112F344AE594CDFE9

TR31 block generated with the following header:
"""""""""""""""""""""""""""""""""""""""""""""""
Keyblock version: B
Key usage: M7 (HMAC)
Algorithm: H (HMAC)
Mode of use: G (Generate only)
Exportability: N (Non-exportable)
No optional fields are added and padding is all zeroes.


Generated using BPTools 20.1
""""""""""""""""""""""""""""
1, Use menu Generic->Key Blocks->TR31 Key Block
2, Choose the HMAC salt and copy the salt to the "Plain Key". Choose a KBPK (Default should be fine)
3, Paste the IPEK under the "Plain Key" edit box
4, Enter the header information as shown below
	Keyblock version: B
	Key usage: M7 (HMAC)
	Algorithm: H (HMAC)
	Mode of use: G (Generate only)
	Exportability: N (Non-exportable)
	No optional fields are added and padding is all zeroes.
5, Generate the TR31 block
6, Save the TR31 block in a text file
7, Now, prepare a binary file of the KBPK key with 4 byte header -> 00 00 80 00 <KBPK>

	# Create .rsa wrapped KBPK file using RSA_ENC_TERM public key
	Encrypt KBPK to .rsa file. Note that the KBPK should be a 20 byte TDES key bin file
	First 2 bytes = 00 00 (Indicates that the data is a TDES key)
	Next 2 bytes = 00 80 (128 bits = 16 bytes of TDES key. The first 8 bytes key is reused for TDES)
	Next 16 bytes = TDES key
	Note that the KBPK file is encrypted using RSA_ENC_TERM "public" key. So, the -pubin parameter is required
	openssl pkeyutl -encrypt -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:sha256 -pubin -inkey RSA_ENC_TERM_public.key -in kbpk.bin -out TDES_RKI_SESSION.rsa 

8, RSA encryrpt the KBPK.bin file using the reader's enc_term public key. This file is the session.rsa
9, The sign the session.rsa using the sign_server private key. This file is the session.rsa.sig

	# Create .sig for the .rsa file using RSA_SERVER private key
	openssl dgst -sha256 -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -sign RSA_SERVER_private.pem -out test.txt.sig test.txt

10, Now, you should have all the files required for SALT injection
