#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Inject the DUKPT keys to CPM
#
# > do_dukpt.sh <key set> <-q/-d> <tty device> 
#
#--------------------------------------------------------------------------------------------------
KEYSET="${1:-dev}"
OPTION1="${2:--d}"
TTY="${3:-/dev/ttyACM0}"
VERSION="0.2"
EXIT_VAL=1
DUKPT_ROOT="./02_dukpt"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [ $# -eq 0 ]; then
   echo "DUKPT Injector v${VERSION}"
   echo "Usage:"
   echo "do_dukpt.sh <key set> <-q/-d> <tty device>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
case $KEYSET in
    dev)
        DUKPT_SOURCE="${DUKPT_ROOT}/01_dev_ks1"
    ;;
    nfp)
        DUKPT_SOURCE="${DUKPT_ROOT}/02_nfp_ks"
    ;;
    *)
        echo "ERROR: unsupported keyset!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -tty $TTY $OPTION1 -skin dukpt
if [ $? -ne 0 ]; then
   echo "ERROR : Start key injection"
   exit $EXIT_VAL
fi
read -p "Pause.. for 1 secs" -t 1

#--------------------------------------------------------------------------------------------------
cd $DUKPT_SOURCE
for file in *; do
   ((EXIT_VAL=EXIT_VAL+1))
   PROCESSED=0
   if [ -f "$file" ]; then
	echo $file
        case $file in
            *server*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wssvr $file
            ;;
            *session.rsa)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wsess $file
            ;;
            *session.rsa.sig)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wsign $file
            ;;
            *tr31*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wtr31dukpt $file
            ;;
            *ksn*)
                FAILED_FILE=$file
                PROCESSED=1
                ../../$EXECUTABLE -tty $TTY $OPTION1 -wksn $file
            ;;
	    *)
                break
	    ;;
        esac
        if [ $? -ne 0 ]; then
            echo "ERROR : ${FAILED_FILE} injection"
            exit $EXIT_VAL
        fi
        if (( PROCESSED == 1 )); then
            read -p "Pause.. for 1 secs" -t 1
            echo ""
        fi
   fi
done

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
../../$EXECUTABLE -tty $TTY $OPTION1 -ekin
if [ $? -ne 0 ]; then
   echo "ERROR : End key injection"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "DUKPT injection complete"
exit 0
#--------------------------------------------------------------------------------------------------
