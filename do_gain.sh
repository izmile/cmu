#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Set RX gain value
#
# > ./do_gain.sh <gain value = 0 to 3> <type> <hid device>
#
#--------------------------------------------------------------------------------------------------
GAIN_ARG="${1:-2}"
TYPE_ARG="${2:-AB}"
HID="${3:-/dev/hidraw0}"
EXIT_VAL=1
VERSION="0.3"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [[ $# == 0 ]]; then
   echo "Set RX gain v${VERSION}"
   echo "Usage:"
   echo "do_gain.sh <gain value = 0 to 3> <type (default = AB)> <hid device (default = /dev/hidraw0)>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $TYPE_ARG == "A" ]]; then
   TYPE="01"
elif [[ $TYPE_ARG == "B" ]]; then
   TYPE="02"
elif [[ $TYPE_ARG == "AB" ]]; then
   TYPE="00"
else
   echo "ERROR: <type> argument shall be A, B or AB"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $GAIN_ARG == "0" ]] || [[ $GAIN_ARG == "00" ]]; then
   GAIN="00"
elif [[ $GAIN_ARG == "1" ]] || [[ $GAIN_ARG == "01" ]]; then
   GAIN="01"
elif [[ $GAIN_ARG == "2" ]] || [[ $GAIN_ARG == "02" ]]; then
   GAIN="02"
elif [[ $GAIN_ARG == "3" ]] || [[ $GAIN_ARG == "03" ]]; then
   GAIN="03"
else
   echo "ERROR: RX gain value shall be 0 to 3"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -hid $HID -hdata 06 00 05 00 $TYPE 00 $GAIN
if [[ $? != 0 ]]; then
   echo "ERROR: Failed to set gain = "$GAIN
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "RX gain value updated"
exit 0
#--------------------------------------------------------------------------------------------------
