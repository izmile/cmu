//-------------------------------------------------------------------------------------------------
// CPM Management Utility (CMU) main application source
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 12/Oct/2019
//-------------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>                                         /* File Control Definitions           */
#include <termios.h>                                       /* POSIX Terminal Control Definitions */
#include <unistd.h>                                        /* UNIX Standard Definitions 	     */ 
#include <errno.h>                                         /* ERROR Number Definitions           */
#include <string.h>
#include <stdlib.h>
#include <wintypes.h>                                      // From /usr/include/PCSC
#include <winscard.h>                                      // From /usr/include/PCSC

#include "common.h"
#include "cmu.h"
#include "cpm_device.h"

//-------------------------------------------------------------------------------------------------
// Global variables
//-------------------------------------------------------------------------------------------------
const char *ErrorString[] = {  /*   0 */ "Operation completed successfully",
                               /*   1 */ "Missing parameters!",
                               /*   2 */ "Unable to open serial device!", 
                               /*   3 */ "Serial device settings failed!",
                               /*   4 */ "Serial device write failed!",
                               /*   5 */ "Serial device read failed!",
                               /*   6 */ "No data from serial device!", 
                               /*   7 */ "Transceive parameters incorrect!",
                               /*   8 */ "Unkown error", 
                               /*   9 */ "Wlink incorrect parameters!", 
                               /*  10 */ "Wlink file open error!", 
                               /*  11 */ "Wlink memory allocation error!", 
                               /*  12 */ "Wlink file loading error!", 
                               /*  13 */ "Insufficient parameters!",
                               /*  14 */ "Multiple actions defined!",
                               /*  15 */ "tty serial device not defined!",
                               /*  16 */ "Unknown option!",
                               /*  17 */ "Wlink RX packet CRC error!",
                               /*  18 */ "Wlink RX packet too small!",
                               /*  19 */ "Wlink operation failed!",
                               /*  20 */ "No action defined!",
                               /*  21 */ "Wlink empty file error!",
                               /*  22 */ "Wlink packet read data error!",
                               /*  23 */ "Wlink file write error!",
                               /*  24 */ "HID device not defined!",
                               /*  25 */ "Non-Hex characters found in HID data!",
                               /*  26 */ "Unable open hidraw device!",
                               /*  27 */ "hidraw write failed!",
                               /*  28 */ "hidraw read failed!",
                               /*  29 */ "No data from hidraw device!",
                               /*  30 */ "hidraw device not open!",
                               /*  31 */ "Incorrect hidraw device parameters!",
                               /*  32 */ "Incorrect parameter!",
                               /*  33 */ "Non-Hex characters found in APDU data!",   
                               /*  34 */ "ScardEstablishContext failed!"    
                               /*  35 */ "ScardConnect failed!"
                               /*  36 */ "ScardTransmit failed!"                            
                             };


const CMU_ERROR_STRING CMU_Error_String[] =  {    { CMU_NO_ERROR, ErrorString[0] },

                                                  { TTY_SERIAL_TTY_OPEN_ERROR, ErrorString[2] }, 
                                                  { TTY_SERIAL_TTY_SETTINGS_ERROR, ErrorString[3] },
                                                  { TTY_SERIAL_WRITE_ERROR, ErrorString[4] },
                                                  { TTY_SERIAL_READ_ERROR, ErrorString[5] },
                                                  { TTY_SERIAL_READ_EOF, ErrorString[6] },
                                                  { TTY_SERIAL_PORT_NOT_OPEN, ErrorString[2] },
                                                  { TTY_SERIAL_TRANSCEIVE_PARAMS_INCORRECT, ErrorString[7] },
                                                  { TTY_SERIAL_UNKNOWN_ERROR, ErrorString[8] },
                                                  { WLINK_PARAMS_ERROR, ErrorString[9] },
                                                  { WLINK_FILE_OPEN_ERROR, ErrorString[10] },
                                                  { WLINK_MALLOC_ERROR, ErrorString[11] },
                                                  { WLINK_FILE_LOADING_ERROR, ErrorString[12] },
                                                  { WLINK_CRC_ERROR, ErrorString[17] },
                                                  { WLINK_RX_PACKET_TOO_SMALL, ErrorString[18] },
                                                  { WLINK_OPERATION_FAILED, ErrorString[19] },
                                                  { WLINK_EMPTY_FILE_ERROR, ErrorString[21] },
                                                  { WLINK_READ_DATA_ERROR, ErrorString[22] },
                                                  { WLINK_FILE_WRITE_ERROR, ErrorString[23] },
      
                                                  { CMU_MISSING_PARAMS, ErrorString[1] },
                                                  { CMU_MULTIPLE_ACTIONS, ErrorString[14] },
                                                  { CMU_INSUFFICIENT_PARAMS, ErrorString[13] },
                                                  { CMU_TTY_PORT_NOT_DEFINED, ErrorString[15] },
                                                  { CMU_UNKNOWN_FLAG, ErrorString[16] },
                                                  { CMU_NO_ACTION_DEFINED, ErrorString[20] },
                                                  { CMU_INCORRECT_PARAMS, ErrorString[32]},

                                                  { CMU_HID_RAW_PATH_NOT_DEFINED, ErrorString[24]},
                                                  { CMU_HID_DATA_NOT_HEX_DIGITS, ErrorString[25]},
                                                  { HIDRAW_OPEN_ERROR, ErrorString[26]},
                                                  { HIDRAW_WRITE_ERROR, ErrorString[27]},
                                                  { HIDRAW_READ_ERROR, ErrorString[28]},
                                                  { HIDRAW_READ_EOF, ErrorString[29]},
                                                  { HIDRAW_NOT_OPEN, ErrorString[30]},
                                                  { HIDRAW_TRANSCEIVE_PARAMS_INCORRECT, ErrorString[31]},

                                                  { CMU_APDU_DATA_NOT_HEX_DIGITS, ErrorString[33]},
                                                  { CMU_ESTABLISH_CONTEXT_FAILED, ErrorString[34]},
                                                  { CMU_SCARD_CONNECT_FAILED, ErrorString[35]},
                                                  { CMU_SCARD_TRANSMIT_FAILED, ErrorString[36]},

                                                  { 0xFFFF, NULL},
                                       };

const char Str_Do_Not_Interrupt[] = ">>> PLEASE DO NOT INTERRUPT <<<";

CMU_SETTINGS CMU_Settings;

//-------------------------------------------------------------------------------------------------
// Prints the application banner
//-------------------------------------------------------------------------------------------------
void Print_Banner()
{
    switch(CMU_Settings.UI_Mode)
    {
        case(CMU_UI_NORMAL_TEXT):
        case(CMU_UI_DEBUG):
        {
            printf("\nCPM Mangement Utility (CMU) v%02X.%02X\n", CMU_MAJOR_VERSION, CMU_MINOR_VERSION);
            break;
        }
        case(CMU_UI_JSON):
        {
            break;
        }
        default:
        case(CMU_UI_STEALTH):
        {
            break;
        }
    }    
    return;
}

//-------------------------------------------------------------------------------------------------
// Prints the application usage
//-------------------------------------------------------------------------------------------------
void Print_Usage()
{
    switch(CMU_Settings.UI_Mode)
    {
        case(CMU_UI_DEBUG):
        case(CMU_UI_NORMAL_TEXT):
        {
            printf("\nUsage:");
            printf("\nCMU <Comm option> <Action>");
            printf("\n\nComm option:");
            printf("\n-tty <device> = CPM wlink serial device (/dev/ttyACMX)");
            printf("\n-hid <device> = CPM management interface (/dev/hidrawX)");
            printf("\n-cpmld <hid device> = CPM loader");
            printf("\n-pcsc <reader name> = PC/SC reader name");
            printf("\n-xtout <mS> = Extra timeout value in milliseconds");
            printf("\n\n-tty action:");
            printf("\n-ufw <file> = Update firmware. <file> = Firmware signed binary file");
            printf("\n-usbl <file> = Update SBL. <file> = SBL signed binary file");
            printf("\n-ulan <file> = Update language setting. <file> = Language signed binary file");
            printf("\n-urev <file> = Update revocation setting. <file> = Revocation signed binary file");
            printf("\n-uexc <file> = Update exception setting. <file> = Exception signed binary file");
            printf("\n-ucak <file> = Update CA keys setting. <file> = CA keys signed binary file");
            printf("\n-uvis <file> = Update VISA setting. <file> = VISA signed binary file");
            printf("\n-umcl <file> = Update MCL setting. <file> = MCL signed binary file");
            printf("\n-uamx <file> = Update AMX setting. <file> = AMX signed binary file");
            printf("\n-udps <file> = Update DPASS setting. <file> = DPASS signed binary file");
            printf("\n-ujcb <file> = Update JCB setting. <file> = DPASS signed binary file");
            printf("\n-ubin <file> = Update bin setting. <file> = Bin signed binary file");
            printf("\n-rsmanf <file> = Read RSA_SIGN_MANUF. <file> = File to save RSA_SIGN_MANUF");
            printf("\n-reterm <file> = Read RSA_ENC_TERM. <file> = File to save RSA_ENC_TERM");
            printf("\n-rsterm <file> = Read RSA_SIGN_TERM. <file> = File to save RSA_SIGN_TERM");
            printf("\n-skin <dukpt / sconf / hmac> = Start key injection for DUKPT / RSA_SIGN_CONFIG / HMAC salt injection");
            printf("\n Following actions require 'Start key injection DUKPT (-skin dukpt)' to be run previously" );
            printf("\n   -rsno <file> = Read serial number. <file> = File to save serial number");
            printf("\n   -wssvr <file> = Write RSA_SIGN_SERVER. <file> = RSA_SIGN_SERVER (.der) certificate file");
            printf("\n   -wsess <file> = Write DUKPT Session. <file> = DUKPT session file");
            printf("\n   -wsign <file> = Write DUKPT Signature. <file> = DUKPT signature file");
            printf("\n   -wtr31dukpt <file> = Write TR31 block for DUKPT. <file> = DUKPT TR31 file");
            printf("\n   -wtr31hmac <file> = Write TR31 block for HMAC salt. <file> = HMAC salt TR31 file");
            printf("\n   -wksn <file> = Write Key Serial Number (KSN). <file> = KSN file");
            printf("\n Following actions require 'Start key injection SCONF (-skin sconf)' to be run previously" );
            printf("\n   -wsconf <file> = Write RSA_SIGN_CONFIG. <file> = RSA_SIGN_CONFIG (.der) certificate file");
            printf("\n-ekin = End key injection");
            printf("\n-rfr<X> = Send Request-for-Request (RFR) command. <X> defines the context of the RFR command as given below");
            printf("\n   -rfr1 = RFR for SIGN_CONF_MAIN");
            printf("\n   -rfr2 = RFR for DUKPT_INIT_PAY");
            printf("\n   -rfr3 = RFR for Terminal settings");
            printf("\n   -rfr4 = RFR for SBL");
            printf("\n   -rfr5 = RFR for FW");
            printf("\n-v = Print CPM firmware version");
            printf("\n\n-hid action:");
            printf("\n-hdata <hex bytes> = HID data bytes as hex bytes (separated by space)");
            printf("\n\n-cpmld action:");
            printf("\n-hfile <hex file> = Hex file path to load");
            printf("\n\n-pcsc action:");
            printf("\n-apdu <hex bytes> = APDU data bytes as hex bytes (separated by space)");
            printf("\n\nUI option:"); 
            printf("\n-n = Normal text mode (default)");
            printf("\n-j = Json text mode");
            printf("\n-q = Quiet mode");
            printf("\n-d = Debug mode");
            printf("\n-h, -help = Prints usage");
            printf("\n");
            break;
        }
        case(CMU_UI_JSON):
        {
            break;
        }
        default:
        case(CMU_UI_STEALTH):
        {
            break;
        }
    } 
    return;
}

//-------------------------------------------------------------------------------------------------
// Returns the error string pointer given the error code
// If no string exists for the error code then a NULL is returned
//-------------------------------------------------------------------------------------------------
const char *Get_Error_String(int _errorCode)
{
    const char *_retval;
    int _index = 0;

    while(CMU_Error_String[_index].String != NULL)
    {
        if(CMU_Error_String[_index].ID == _errorCode)
        {
            break;
        }
        _index++;
    } 
    _retval = CMU_Error_String[_index].String;
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Prints error messages
//-------------------------------------------------------------------------------------------------
void Print_Error(int _errorCode)
{
    //printf("\nError code = %d", _errorCode);
    const char *_ErrorString;
    switch(CMU_Settings.UI_Mode)
    {
        case(CMU_UI_STEALTH):
        {
            break;
        }
        case(CMU_UI_JSON):
        {
            break;
        }
        case(CMU_UI_DEBUG):
        case(CMU_UI_NORMAL_TEXT):
        default:
        {
            if(_errorCode == CMU_PRINT_USAGE)
            {
                Print_Usage();
            }
            else
            {
                if(_errorCode)
                {
                    printf("\nError %d: ",  _errorCode);
                }
                else
                {
                    printf("\n");
                } 
                _ErrorString = Get_Error_String(_errorCode);
                if(_ErrorString)
                {
                    printf("%s\n", _ErrorString);
                }
            }
            break;
        }
    } 
    printf("\n");
    return;
}

#if 0
//-------------------------------------------------------------------------------------------------
// Prints UI message depending on the UI mode
//-------------------------------------------------------------------------------------------------
void Print_UI_Message(char *_Message)
{
    switch(CMU_Settings.UI_Mode)
    {
        case(CMU_UI_STEALTH):
        {
            break;
        }
        case(CMU_UI_JSON):
        {
            break;
        }
        case(CMU_UI_DEBUG):
        case(CMU_UI_NORMAL_TEXT):
        default:
        {
            printf("%s")
            break;
        }
    } 
}
#endif

//-------------------------------------------------------------------------------------------------
// Parses the command line and extracts the required parameters
//-------------------------------------------------------------------------------------------------
int Parse_CLI(int _argc, char *_argv[])
{
    int _retval = CMU_NO_ERROR;
	int _index;
    bool _action_defined = false;
    CMU_Settings.UI_Mode = CMU_UI_NORMAL_TEXT;
    CMU_Settings.tty_Device = NULL;
    CMU_Settings.Update_File_Path = NULL;
    CMU_Settings.hidraw_Device = NULL;
    CMU_Settings.Key_Injection_Type = WLINK_KEY_INJECTION_DUKPT;
    CMU_Settings.extra_timeout_mS = 0;
    CMU_Settings.PCSC_Reader_Name = NULL;
    CMU_Settings.APDU_Size = 0;

    do
    {
        if(_argc < CMU_MIN_ARGS_REQUIRED)
        {
            if((_argc == 1) || (_argc > 1 && (!strcasecmp(_argv[1], "-help") || !strcasecmp(_argv[1], "-h"))))
            {
                _retval = CMU_PRINT_USAGE;
            }
            else
            {
                _retval = CMU_MISSING_PARAMS;
            }            
            break;
        }

        for(_index = 1; _index < _argc; _index++)
        {
            if(*_argv[_index] == '-')
            {
                if(!strcasecmp(_argv[_index] + 1, "v"))
                {
                    CMU_Settings.Action = CMU_ACTION_WLINK_GET_DESCTIPTOR;
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }                
                }
                else if(!strcasecmp(_argv[_index] + 1, "ufw"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_FW;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "usbl"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_SBL;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }        
                }
                else if(!strcasecmp(_argv[_index] + 1, "ulan"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_LANGUAGE;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "urev"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_REVOCATION;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }            
                else if(!strcasecmp(_argv[_index] + 1, "uexc"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_EXCEPTION;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }    
                else if(!strcasecmp(_argv[_index] + 1, "ucak"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_CA_KEYS;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }    
                else if(!strcasecmp(_argv[_index] + 1, "ubin"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_BIN;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "uvis"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_PW;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }  
                else if(!strcasecmp(_argv[_index] + 1, "umcl"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_MCL;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "uamx"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_AMX;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }          
                else if(!strcasecmp(_argv[_index] + 1, "udps"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_DPASS;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }  
                else if(!strcasecmp(_argv[_index] + 1, "ujcb"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_LOAD_TSET_JCB;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }  
                else if(!strcasecmp(_argv[_index] + 1, "rsmanf"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_READ_RSA_SIGN_MANUF;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }        
                else if(!strcasecmp(_argv[_index] + 1, "reterm"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_READ_RSA_ENC_TERM;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }      
                else if(!strcasecmp(_argv[_index] + 1, "rsterm"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_READ_RSA_SIGN_TERM;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "skin"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_START_KEY_INJECTION;
                    if(_index < _argc  - 1)
                    {
                        if(!strcasecmp(_argv[_index + 1], "dukpt"))
                        {
                            CMU_Settings.Key_Injection_Type = WLINK_KEY_INJECTION_DUKPT;
                        }
                        else if(!strcasecmp(_argv[_index + 1], "sconf"))
                        {
                            CMU_Settings.Key_Injection_Type = WLINK_KEY_INJECTION_SIGN_CONF;
                        }
                        else if(!strcasecmp(_argv[_index + 1], "hmac"))
                        {
                            CMU_Settings.Key_Injection_Type = WLINK_KEY_INJECTION_HMAC_SALT;
                        }
                        else
                        {
                            _retval = CMU_INCORRECT_PARAMS;
                            break;
                        }                        
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                } 
                else if(!strcasecmp(_argv[_index] + 1, "rsno"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_READ_SERIAL_NUMBER;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }                           
                else if(!strcasecmp(_argv[_index] + 1, "wssvr"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_RSA_SIGN_SERVER;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }        
                else if(!strcasecmp(_argv[_index] + 1, "wsess"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_SESSION;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "wsign"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_SESSION_SIGN;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "wtr31dukpt"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_TR31_DUKPT;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "wtr31hmac"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_TR31_HMAC_SALT;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "wksn"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_KSN;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "wsconf"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_WRITE_RSA_SIGN_CONFIG;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "ekin"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_END_KEY_INJECTION;
                }
                else if(!strncasecmp(_argv[_index] + 1, "rfr", 3))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_WLINK_SEND_RFR_COMMAND;
                    switch(*(_argv[_index] + 4))
                    {
                        case('1'):
                        {
                            CMU_Settings.RFR_type = 0x01;                            
                            break;
                        }
                        case('2'):
                        {
                            CMU_Settings.RFR_type = 0x02;                            
                            break;
                        }
                        case('3'):
                        {
                            CMU_Settings.RFR_type = 0x11;                            
                            break;
                        }
                        case('4'):
                        {
                            CMU_Settings.RFR_type = 0x21;                            
                            break;
                        }
                        case('5'):
                        {
                            CMU_Settings.RFR_type = 0x22;                            
                            break;
                        }
                        default:
                        {
                            _action_defined = false;
                            break;
                        } 
                    }                                                                 
                }                                                                                
                else if(!strcasecmp(_argv[_index] + 1, "q"))
                {
                    CMU_Settings.UI_Mode = CMU_UI_STEALTH;
                }
                else if(!strcasecmp(_argv[_index] + 1, "j"))
                {
                    //CMU_Settings.UI_Mode = CMU_UI_JSON;
                    CMU_Settings.UI_Mode = CMU_UI_NORMAL_TEXT;
                }
                else if(!strcasecmp(_argv[_index] + 1, "n"))
                {
                    CMU_Settings.UI_Mode = CMU_UI_NORMAL_TEXT;
                }
                else if(!strcasecmp(_argv[_index] + 1, "d"))
                {
                    CMU_Settings.UI_Mode = CMU_UI_DEBUG;
                }   
                else if(!strcasecmp(_argv[_index] + 1, "xtout"))
                {
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.extra_timeout_mS = atoi(_argv[_index + 1]);
                        _index++;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }               
                } 
                else if(!strcasecmp(_argv[_index] + 1, "tty"))
                {
                    if(CMU_Settings.hidraw_Device == NULL && CMU_Settings.PCSC_Reader_Name == NULL)
                    {
                        if(_index < _argc  - 1)
                        {
                            CMU_Settings.tty_Device = _argv[_index + 1];
                            _index++;
                        }
                        else
                        {
                            _retval = CMU_INSUFFICIENT_PARAMS;
                            break;
                        }
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }
                    
                }
                else if(!strcasecmp(_argv[_index] + 1, "hid"))
                {
                    if(CMU_Settings.tty_Device == NULL && CMU_Settings.PCSC_Reader_Name == NULL)
                    {
                        if(_index < _argc  - 1)
                        {
                            CMU_Settings.hidraw_Device = _argv[_index + 1];
                            _index++;
                        }
                        else
                        {
                            _retval = CMU_INSUFFICIENT_PARAMS;
                            break;
                        }
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "hdata"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_HID_MGMT_COMMAND;
                    if(_index < _argc  - 1)
                    {
                        if(!Parse_CLI_Hex_Data(&_argv[_index + 1], _argc - _index - 1, CMU_Settings.hid_data, sizeof(CMU_Settings.hid_data)))
                        {
                            _retval = CMU_HID_DATA_NOT_HEX_DIGITS;
                        }
                        // Once we hit the hid_data part we do not parse any more command line args.
                        // All command line args after -hdata are treated as hex value bytes
                        break;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                } 
                // Not implemente yet
                else if(!strcasecmp(_argv[_index] + 1, "hfile"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    } 
                    CMU_Settings.Action = CMU_ACTION_HID_LOAD_HEXFILE;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.Update_File_Path = _argv[_index + 1];
                        break;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "pcsc"))
                {
                    if(CMU_Settings.hidraw_Device == NULL && CMU_Settings.tty_Device == NULL)
                    {
                        if(_index < _argc  - 1)
                        {
                            CMU_Settings.PCSC_Reader_Name = _argv[_index + 1];
                            _index++;
                        }
                        else
                        {
                            _retval = CMU_INSUFFICIENT_PARAMS;
                            break;
                        }
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }
                }
                else if(!strcasecmp(_argv[_index] + 1, "apdu"))
                {
                    if(!_action_defined)
                    {
                        _action_defined = true;
                    }
                    else
                    {
                        _retval = CMU_MULTIPLE_ACTIONS;
                        break;
                    }  
                    CMU_Settings.Action = CMU_ACTION_PCSC_TRANSCEIVE_APDU;
                    if(_index < _argc  - 1)
                    {
                        CMU_Settings.APDU_Size = Parse_CLI_Hex_Data(&_argv[_index + 1], _argc - _index - 1, CMU_Settings.APDU_Data, sizeof(CMU_Settings.APDU_Data));
                        if(!CMU_Settings.APDU_Size)
                        {
                            _retval = CMU_APDU_DATA_NOT_HEX_DIGITS;
                        }
                        // Once we hit the hid_data part we do not parse any more command line args.
                        // All command line args after -hdata are treated as hex value bytes
                        break;
                    }
                    else
                    {
                        _retval = CMU_INSUFFICIENT_PARAMS;
                        break;
                    }
                }
                else
                {
                    //printf("\n%s", _argv[_index]);
                    _retval = CMU_UNKNOWN_FLAG;
                    break;
                }                         
            }  // End of if(*_argv[_index] == '-')
        } // End of for(_index = 1; _index < _argc; _index++)

        if(_retval != CMU_NO_ERROR)
        {
            break;
        }

        if((CMU_Settings.Action & 0xF0) == 0x00 && CMU_Settings.tty_Device == NULL)
        {
            _retval = CMU_TTY_PORT_NOT_DEFINED;
        }

        if((CMU_Settings.Action & CMU_ACTION_HID_MGMT_COMMAND) == CMU_ACTION_HID_MGMT_COMMAND && CMU_Settings.hidraw_Device == NULL)
        {
            _retval = CMU_HID_RAW_PATH_NOT_DEFINED;
        }

        if(!_action_defined)
        {
            _retval = CMU_NO_ACTION_DEFINED;
        }
    }while(0);
	return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Converts a 2 digit hex character string to 1 byte value
// Retuns a short value. The byte value is in the LSB. If there are non-hex characters encountered
// then the MSB will have a non zero value indicating an error condition
//-------------------------------------------------------------------------------------------------
short Str2Hex(char *_byte)
{
    short _retval = 0, _error = 0;
    int _index, _shift;
    for(_index = 0, _shift = 4; _index < 2; _index++, _shift -= 4, _byte++)
    {
        if(*_byte >= '0' && *_byte <= '9')
        {
            _retval |= *_byte - '0';
        }
        else if(*_byte >= 'A' && *_byte <= 'F')
        {
            _retval |= *_byte - 'A' + 10;
        }
        else if(*_byte >= 'a' && *_byte <= 'f')
        {
            _retval |= *_byte - 'a' + 10;
        }
        else
        {
            _error = 0x0100;
        }
        
        _retval <<= _shift;
    }
    return(_retval | _error);
}

//-------------------------------------------------------------------------------------------------
// Parse the hid data command line args and fills up the Data_Buffer with hex values
//-------------------------------------------------------------------------------------------------
int Parse_CLI_Hex_Data(char *_argv[], unsigned int _argc, char *_Data_Buffer, int _Size)
{
    int _retval = 0;
    char *_ptr;
    int _index;
    short _val;

    memset(_Data_Buffer, 0, _Size);

    for(_index = 0; _index < _argc; _index++)
    {
        _ptr = _argv[_index];
        _val = Str2Hex(_ptr);
        if(_val & 0xFF00)  // No error during Str2Hex conversion
        {
            _retval = 0;
            break;
        }
        *_Data_Buffer++ = (char) (_val & 0x00FF);  
        _retval++;      
    }

    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Writes a file into the CPM unit over Wlink interface
//-------------------------------------------------------------------------------------------------
int Write_File(class WLINK *_Wlink, int _File_ID, int _extra_tout_mS)
{
    int _retval;
    do
    {
        _retval = _Wlink->Open(CMU_Settings.tty_Device);
        if(_retval)
        {
            break;
        }
        _retval = _Wlink->Write_File(_File_ID, CMU_Settings.Update_File_Path, _extra_tout_mS);
        _Wlink->Close();
        if(_retval)
        {
            break;
        }
    }while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Reads a file from the CPM unit over Wlink interface
//-------------------------------------------------------------------------------------------------
int Read_File(class WLINK *_Wlink, int _File_ID, int _extra_tout_mS)
{
    int _retval;
    do
    {
        _retval = _Wlink->Open(CMU_Settings.tty_Device);
        if(_retval)
        {
            break;
        }
        _retval = _Wlink->Read_File(_File_ID, CMU_Settings.Update_File_Path, _extra_tout_mS);
        _Wlink->Close();
        if(_retval)
        {
            break;
        }
    }while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Issues key injection start or end command
//-------------------------------------------------------------------------------------------------
int Key_Injection(class WLINK *_Wlink, KEY_INJECTION_TYPE _key_type, bool _isStart, int _extra_tout_mS)
{
    int _retval;
    do
    {
        _retval = _Wlink->Open(CMU_Settings.tty_Device);
        if(_retval)
        {
            break;
        }
        if(_isStart)
        {
            _retval = _Wlink->Key_Injection_Init(_key_type, _extra_tout_mS);
        }
        else
        {
            _retval = _Wlink->Key_Injection_End(_extra_tout_mS);
        }        
        _Wlink->Close();
        if(_retval)
        {
            break;
        }
    }while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// Issues RFR command
//-------------------------------------------------------------------------------------------------
int RFR_Command(class WLINK *_Wlink, char _RFR_Type, int _extra_tout_mS)
{
    int _retval;
    do
    {
        _retval = _Wlink->Open(CMU_Settings.tty_Device);
        if(_retval)
        {
            break;
        }
        _retval = _Wlink->RFR_Command((uint8_t)_RFR_Type, _extra_tout_mS);      
        _Wlink->Close();
        if(_retval)
        {
            break;
        }
    }while(0);
    return(_retval);
}

//-------------------------------------------------------------------------------------------------
// main function
//-------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    int _retval = CMU_NO_ERROR;
    CPM_DEVICE _CPM;
    CMU_Settings.UI_Mode = CMU_UI_NORMAL_TEXT;
    do
    {
        _retval = Parse_CLI(argc, argv);

        switch(CMU_Settings.UI_Mode)
        {
            case(CMU_UI_DEBUG):
            case(CMU_UI_NORMAL_TEXT):
            default:
            {
                Print_Banner();
                break;
            }
        }

        if(_retval)
        {
            break;
        }

        // printf("\nUI mode = %d", CMU_Settings.UI_Mode);
        // printf("\nAction = %d", CMU_Settings.Action);
        // printf("\ntty = %s", CMU_Settings.tty_Device);
        // printf("\nUpdate file = %s", CMU_Settings.Update_File_Path);
        // printf("\n%u %s %s", argc, argv[1], argv[2]);

        _CPM.Wlink.Set_UI_Mode(CMU_Settings.UI_Mode);
        switch(CMU_Settings.Action)
        {
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_GET_DESCTIPTOR):
            {
                 _retval = _CPM.Wlink.Open(CMU_Settings.tty_Device);
                if(_retval)
                {
                    break;
                }
                _retval = _CPM.Wlink.Get_Descriptor(CMU_Settings.extra_timeout_mS);
                _CPM.Wlink.Close();
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_FW):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating FW...");
                    printf("\n%s\n", Str_Do_Not_Interrupt);
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_FW, CMU_Settings.extra_timeout_mS);
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_SBL):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating SBL...");
                    printf("\n%s\n", Str_Do_Not_Interrupt);
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_SBL, CMU_Settings.extra_timeout_mS);
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_LANGUAGE):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - Language...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_LANGUAGE, CMU_Settings.extra_timeout_mS);
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_REVOCATION):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - Revocation...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_REVOCATION, CMU_Settings.extra_timeout_mS);                
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_EXCEPTION):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - Exception...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_EXCEPTION, CMU_Settings.extra_timeout_mS);                       
                break;
            }  
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_CA_KEYS):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - CA keys...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_EMV_CA, CMU_Settings.extra_timeout_mS);                       
                break;
            }
            //-------------------------------------------------------------------------------------             
            case(CMU_ACTION_WLINK_LOAD_TSET_BIN):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - Bin list...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_BIN_LIST, CMU_Settings.extra_timeout_mS);                       
                break;
            }
            //-------------------------------------------------------------------------------------  
            case(CMU_ACTION_WLINK_LOAD_TSET_PW):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - Paywave...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_PAYWAVE_SETTINGS, CMU_Settings.extra_timeout_mS);        
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_MCL):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - MCL...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_MCL_SETTINGS, CMU_Settings.extra_timeout_mS);        
                break;
            }
            //-------------------------------------------------------------------------------------          
            case(CMU_ACTION_WLINK_LOAD_TSET_AMX):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - AMX...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_AXP_SETTINGS, CMU_Settings.extra_timeout_mS); 
                break;
            }
            //-------------------------------------------------------------------------------------            
            case(CMU_ACTION_WLINK_LOAD_TSET_DPASS):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - DPASS...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_DPASS_SETTINGS, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_LOAD_TSET_JCB):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nUpdating terminal settings - JCB...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_JCB_SETTINGS, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------             
            case(CMU_ACTION_WLINK_READ_RSA_SIGN_MANUF):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nReading RSA_SIGN_MANUF...\n");
                }
                _retval = Read_File(&_CPM.Wlink, WLINK_FILE_ID_RSA_SIGN_MANUF, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------   
            case(CMU_ACTION_WLINK_READ_RSA_ENC_TERM):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nReading RSA_ENC_TERM...\n");
                }
                _retval = Read_File(&_CPM.Wlink, WLINK_FILE_ID_RSA_ENC_TERM, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_READ_RSA_SIGN_TERM):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nReading RSA_SIGN_TERM...\n");
                }
                _retval = Read_File(&_CPM.Wlink, WLINK_FILE_ID_RSA_SIGN_TERM, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_START_KEY_INJECTION):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nStart key injection: ");
                    switch(CMU_Settings.Key_Injection_Type)
                    {
                        case(WLINK_KEY_INJECTION_DUKPT):
                        {
                            printf("DUKPT");
                             break;
                        }
                        case(WLINK_KEY_INJECTION_SIGN_CONF):
                        {
                            printf("RSA_SIGN_CONFIG");
                            break;
                        }
                        case(WLINK_KEY_INJECTION_HMAC_SALT):
                        {
                            printf("HMAC salt");
                            break;
                        }
                    }
                    printf("\n");
                }
                _retval = Key_Injection(&_CPM.Wlink, CMU_Settings.Key_Injection_Type, true, CMU_Settings.extra_timeout_mS);
                break;
            }
            //-------------------------------------------------------------------------------------   
            case(CMU_ACTION_WLINK_READ_SERIAL_NUMBER):
            {                
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nReading Serial number...\n");
                }
                _retval = Read_File(&_CPM.Wlink, WLINK_FILE_ID_SERIAL_NUMBER, CMU_Settings.extra_timeout_mS);                 
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_RSA_SIGN_SERVER):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting RSA_SIGN_SERVER (.der) certificate...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_RSA_SIGN_SERVER, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_SESSION):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting Session...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_KI_SESSION, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_SESSION_SIGN):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting Session signature...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_KI_SESSION_SIGN, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_TR31_DUKPT):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting TR31 block...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_KI_DUKPT_PAY, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_TR31_HMAC_SALT):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting TR31 block...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINT_FILE_ID_KI_HMAC_SALT, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------            
            case(CMU_ACTION_WLINK_WRITE_KSN):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting Key Serial Number (KSN)...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_KI_DUKPT_PAY_KSN, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_WRITE_RSA_SIGN_CONFIG):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nWriting RSA_SIGN_CONFIG (.der) certificate...\n");
                }
                _retval = Write_File(&_CPM.Wlink, WLINK_FILE_ID_RSA_SIGN_CONF_MAIN, CMU_Settings.extra_timeout_mS);      
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_END_KEY_INJECTION):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nEnd key injection...\n");
                }
                _retval = Key_Injection(&_CPM.Wlink, CMU_Settings.Key_Injection_Type, false, CMU_Settings.extra_timeout_mS);  // Arg #2 is ignored anyway
                break;
            }
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_WLINK_SEND_RFR_COMMAND):
            {
                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nReqest-for-Request command...\n");
                }
                _retval = RFR_Command(&_CPM.Wlink, CMU_Settings.RFR_type, CMU_Settings.extra_timeout_mS);  // Arg #2 is ignored anyway
                break;
            }            
            //-------------------------------------------------------------------------------------
            case(CMU_ACTION_HID_MGMT_COMMAND):
            {
                char _rxbuffer[65];
                int _bytes_read;

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nHIDraw : %s", CMU_Settings.hidraw_Device);
                }

                _retval = _CPM.HIDraw.Open(CMU_Settings.hidraw_Device);
                if(_retval)
                {
                    break;
                }
                // _retval = _CPM.HIDraw.Write(0, CMU_Settings.hid_data, sizeof(CMU_Settings.hid_data));

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\n>> [%lu] : ", sizeof(CMU_Settings.hid_data));
                    for(int i=0; i < sizeof(CMU_Settings.hid_data); i++)
                    {
                        printf("%02X ", CMU_Settings.hid_data[i] & 0xFF);
                    }
                }

                _bytes_read = sizeof(_rxbuffer);
                _retval = _CPM.HIDraw.Transceive(0, CMU_Settings.hid_data, sizeof(CMU_Settings.hid_data), _rxbuffer, &_bytes_read, 
                                                 CMU_HID_WRITE_TO_READ_DELAY_mS + CMU_Settings.extra_timeout_mS, CMU_HID_WRITE_TO_READ_RETRIES);

                if(_retval)
                {
                    break;
                }

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\n<< [%u] : ", _bytes_read);
                    for(int i=0; i < _bytes_read; i++)
                    {
                        printf("%02X ", _rxbuffer[i] & 0xFF);
                    }
                }

                _CPM.HIDraw.Close();
                break;
            }
            //------------------------------------------------------------------------------------- 
            case(CMU_ACTION_PCSC_TRANSCEIVE_APDU):
            {
                BYTE _rxbuffer[1024];
                int _bytes_read;
                SCARDCONTEXT _hContext;
                SCARDHANDLE _hCard;
                DWORD _dwActiveProtocol, _dwRecvLength;
                SCARD_IO_REQUEST _pioSendPci;
                long _status;

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\nPCSC Reader Name : %s", CMU_Settings.PCSC_Reader_Name);
                }

                _status = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_hContext);
                if(_status != SCARD_S_SUCCESS)
                {
                    _retval = CMU_ESTABLISH_CONTEXT_FAILED;
                    break;
                }

                _status = SCardConnect(_hContext, CMU_Settings.PCSC_Reader_Name, SCARD_SHARE_EXCLUSIVE,
                                 SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &_hCard, &_dwActiveProtocol);

                if(_status != SCARD_S_SUCCESS)
                {
                    _retval = CMU_SCARD_CONNECT_FAILED;
                    break;
                }

                switch(_dwActiveProtocol)
                {
                    case SCARD_PROTOCOL_T0:
                        _pioSendPci = *SCARD_PCI_T0;
                        break;

                    case SCARD_PROTOCOL_T1:
                         _pioSendPci = *SCARD_PCI_T1;
                        break;
                }

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\n>> [%u] : ", CMU_Settings.APDU_Size);
                    for(int i=0; i < CMU_Settings.APDU_Size; i++)
                    {
                        printf("%02X ", CMU_Settings.APDU_Data[i] & 0xFF);
                    }
                }

                _dwRecvLength = sizeof(_rxbuffer);
                _status = SCardTransmit(_hCard, &_pioSendPci, (LPCBYTE)CMU_Settings.APDU_Data, CMU_Settings.APDU_Size,
                                    NULL, _rxbuffer, &_dwRecvLength);

                if(_status != SCARD_S_SUCCESS)
                {
                    _retval = CMU_SCARD_TRANSMIT_FAILED;
                    break;
                }

                if(CMU_Settings.UI_Mode == CMU_UI_DEBUG || 
                   CMU_Settings.UI_Mode == CMU_UI_NORMAL_TEXT)
                {
                    printf("\n<< [%u] : ", (unsigned int)_dwRecvLength);
                    for(int i=0; i < _dwRecvLength; i++)
                    {
                        printf("%02X ", _rxbuffer[i] & 0xFF);
                    }
                }
                break;
            }
            //-------------------------------------------------------------------------------------        
        }

    }while(0);

    Print_Error(_retval);
    return(_retval);
}
