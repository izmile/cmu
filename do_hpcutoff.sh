#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Setup RX highpass cutoff value
#
# > ./do_hpcutoff.sh <cutoff value = 0 to 3> <type> <hid device>
#
#--------------------------------------------------------------------------------------------------
HPCUTOFF_ARG="${1:-0}"
TYPE_ARG="${2:-AB}"
HID="${3:-/dev/hidraw0}"
EXIT_VAL=1
VERSION="0.3"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [[ $# == 0 ]]; then
   echo "Set RX highpass cutoff v${VERSION}"
   echo "Usage:"
   echo "do_hpcutoff.sh <cutoff value = 0 to 3> <type (default = AB)> <hid device (default = /dev/hidraw0)>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $TYPE_ARG == "A" ]]; then
   TYPE="01"
elif [[ $TYPE_ARG == "B" ]]; then
   TYPE="02"
elif [[ $TYPE_ARG == "AB" ]]; then
   TYPE="00"
else
   echo "ERROR: <type> argument shall be A, B or AB"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $HPCUTOFF_ARG == "0" ]] || [[ $HPCUTOFF_ARG == "00" ]]; then
   HPCUTOFF="00"
elif [[ $HPCUTOFF_ARG == "1" ]] || [[ $HPCUTOFF_ARG == "01" ]]; then
   HPCUTOFF="01"
elif [[ $HPCUTOFF_ARG == "2" ]] || [[ $HPCUTOFF_ARG == "02" ]]; then
   HPCUTOFF="02"
elif [[ $HPCUTOFF_ARG == "3" ]] || [[ $HPCUTOFF_ARG == "03" ]]; then
   HPCUTOFF="03"
else
   echo "ERROR: RX highpass cuttof value shall be 0 to 3!"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -hid $HID -hdata 06 00 06 00 $TYPE 00 $HPCUTOFF
if [[ $? != 0 ]]; then
   echo "ERROR: Failed to set highpass cutoff = "$HPCUTOFF
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "RX highpass cutoff value updated"
exit 0
#--------------------------------------------------------------------------------------------------

