BDK: C1D0F8FB4958670DBA40AB1F3752EF0D
KSN: FFFF9876543210000000
IPEK: EFD8979057E7B4A17330EC39466B862E
KBPK: DFF980FC9A78C3AC1EF745AF56BC8AEA

TR31 block generated with the following header:
"""""""""""""""""""""""""""""""""""""""""""""""
Keyblock version: B
Key usage: B1 (Initial DUKPT key)
Algorithm: T (TDES / Triple DES)
Mode of use: X (Key used to derive other keys)
Exportability: N (Non-exportable)
No optional fields are added and padding is all zeroes.


Generated using BPTools 20.1
""""""""""""""""""""""""""""
1, Use menu Payments-> DUKPT (ISO9797)
2, Enter the BDK and the KSN. (Defaults should be fine if you are after a random DUKPT key)
3, Generate the IPEK. The IPEK and PEK are the same for this option
4, Then copy the IPEK key to clipboard
5, Use menu Generic->Key Blocks->TR31 Key Block
6, Choose a KBPK (Default should be fine)
7, Paste the IPEK under the "Plain Key" edit box
8, Enter the header information as shown below
	Keyblock version: B
	Key usage: B1 (Initial DUKPT key)
	Algorithm: T (TDES / Triple DES)
	Mode of use: X (Key used to derive other keys)
	Exportability: N (Non-exportable)
	No optional fields are added and padding is all zeroes.
9, Generate the TR31 block
10, Save the TR31 block in a text file
11, Now, prepare a binary file of the KBPK key with 4 byte header -> 00 00 80 00 <KBPK>

	# Create .rsa wrapped KBPK file using RSA_ENC_TERM public key
	Encrypt KBPK to .rsa file. Note that the KBPK should be a 20 byte TDES key bin file
	First 2 bytes = 00 00 (Indicates that the data is a TDES key)
	Next 2 bytes = 00 80 (128 bits = 16 bytes of TDES key. The first 8 bytes key is reused for TDES)
	Next 16 bytes = TDES key
	Note that the KBPK file is encrypted using RSA_ENC_TERM "public" key. So, the -pubin parameter is required
	openssl pkeyutl -encrypt -pkeyopt rsa_padding_mode:oaep -pkeyopt rsa_oaep_md:sha256 -pubin -inkey RSA_ENC_TERM_public.key -in kbpk.bin -out TDES_RKI_SESSION.rsa 

12, RSA encryrpt the KBPK.bin file using the reader's enc_term public key. This file is the session.rsa
13, The sign the session.rsa using the sign_server private key. This file is the session.rsa.sig

	# Create .sig for the .rsa file using RSA_SERVER private key
	openssl dgst -sha256 -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -sign RSA_SERVER_private.pem -out test.txt.sig test.txt

14, Now, you should have all the files required for DUKPT injection
