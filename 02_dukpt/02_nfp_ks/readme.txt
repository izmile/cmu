BDK: 6CEBADBA69612480FC2FC331D291B0F1
KSN: FFFF9876543210000000
IPEK: D4986676F646B7CA87E4634FCCBDCE2E
KBPK: 0FC5C3A15B6F868D9E47A5E0D9824826

TR31 block generated with the following header:
"""""""""""""""""""""""""""""""""""""""""""""""
Keyblock version: B
Key usage: B1 (Initial DUKPT key)
Algorithm: T (TDES / Triple DES)
Mode of use: X (Key used to derive other keys)
Exportability: N (Non-exportable)
No optional fields are added and padding is all zeroes.


Generated using BPTools 20.1
""""""""""""""""""""""""""""
1, Use menu Payments-> DUKPT (ISO9797)
2, Enter the BDK and the KSN. (Defaults should be fine if you are after a random DUKPT key)
3, Generate the IPEK. The IPEK and PEK are the same for this option
4, Then copy the IPEK key to clipboard
5, Use menu Generic->Key Blocks->TR31 Key Block
6, Choose a KBPK (Default should be fine)
7, Paste the IPEK under the "Plain Key" edit box
8, Enter the header information as shown below
	Keyblock version: B
	Key usage: B1 (Initial DUKPT key)
	Algorithm: T (TDES / Triple DES)
	Mode of use: X (Key used to derive other keys)
	Exportability: N (Non-exportable)
	No optional fields are added and padding is all zeroes.
9, Generate the TR31 block
10, Save the TR31 block in a text file
11, Now, prepare a binary file of the KBPK key with 4 byte header -> 00 00 80 00 <KBPK>
12, RSA encryupt the KBPK.bin file using the reader's enc_term public key. This file is the session.rsa
13, The sign the session.rsa using the sign_server private key. This file is the session.rsa.sig
14, Now, you should have all the files required for DUKPT injection
