#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Loads SBL_Killer to erase the CPM reader
#
# > ./do_killcpm.sh <keyset>
#
#    <keyset>      = 'prod' for production keyset
#                  = 'nfp' for not-for-production keyset
#                  = 'dev' for dev keyset
#                  = 'dev0' for old dev keyset
#--------------------------------------------------------------------------------------------------
KEYSET="${1:-none}"
OPTION1="${2:--d}"
TTY="${3:-/dev/ttyACM0}"
EXIT_VAL=1
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [ "$KEYSET" = "prod" ]; then
   ./$EXECUTABLE -tty "$TTY" OPTION1 -usbl ./04_sbl_killer/SBL_Killer_Access_Prod_Keyset.sbin
elif [ "$KEYSET" = "nfp" ]; then
   ./$EXECUTABLE -tty "$TTY" OPTION1 -usbl ./04_sbl_killer/SBL_Killer_Access_NFP_Keyset.sbin
elif [ "$KEYSET" = "dev" ]; then
   ./$EXECUTABLE -tty "$TTY" OPTION1 -usbl ./04_sbl_killer/SBL_Killer_Dev_Keyset_1.sbin
elif [ "$KEYSET" = "dev0" ]; then
   ./$EXECUTABLE -tty "$TTY" OPTION1 -usbl ./04_sbl_killer/SBL_Killer_Dev_Keyset_1.sbin
else
   echo
   echo "Usage:"
   echo "killcpm.sh <keyset>"
   echo
   echo "<keyset>      = 'prod' for production keyset"
   echo "              = 'nfp' for not-for-production keyset"
   echo "              = 'dev' for dev keyset"
   echo "              = 'dev0' for old dev keyset"
   echo
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
exit 0
#--------------------------------------------------------------------------------------------------

