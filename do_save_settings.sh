#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Save settings
#
# > ./do_save_settings.sh <hid device>
#
#--------------------------------------------------------------------------------------------------
HID="${1:-/dev/hidraw0}"
EXIT_VAL=1
VERSION="0.3"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
echo "Save Settings v${VERSION}"

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -hid $HID -hdata 06 00 21 55 5A A5 AA
if [ $? -ne 0 ]; then
   echo "ERROR: Failed to save settings"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "Save settings successful"
exit 0
#--------------------------------------------------------------------------------------------------
