//-------------------------------------------------------------------------------------------------
// CPM Management Utility (CMU) main application declerations
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 12/Oct/2019
//-------------------------------------------------------------------------------------------------
#ifndef _CMU_H_
#define _CMU_H_

#include "cpm_device.h"

#define CMU_MAJOR_VERSION                                  0x00      // BCD value
#define CMU_MINOR_VERSION                                  0x10      // BCD value
#define CMU_MIN_ARGS_REQUIRED                              3

#define CMU_HID_WRITE_TO_READ_DELAY_mS                     100
#define CMU_HID_WRITE_TO_READ_RETRIES                      3

enum CMU_RETVAL
{
    CMU_NO_ERROR,
    // Extend error code from CPM_DEVICE_UNKNOWN_ERROR
    CMU_MISSING_PARAMS                                     = CPM_DEVICE_UNKNOWN_ERROR + 1,
    CMU_MULTIPLE_ACTIONS,
    CMU_INSUFFICIENT_PARAMS,
    CMU_TTY_PORT_NOT_DEFINED,
    CMU_PRINT_USAGE,
    CMU_NO_ACTION_DEFINED,
    CMU_UNKNOWN_FLAG,
    CMU_HID_RAW_PATH_NOT_DEFINED,
    CMU_HID_DATA_NOT_HEX_DIGITS,
    CMU_APDU_DATA_NOT_HEX_DIGITS,
    CMU_INCORRECT_PARAMS,
    CMU_ESTABLISH_CONTEXT_FAILED,
    CMU_SCARD_CONNECT_FAILED,
    CMU_SCARD_TRANSMIT_FAILED,
};

enum CMU_UI_MODE
{
    CMU_UI_STEALTH,
    CMU_UI_JSON,
    CMU_UI_NORMAL_TEXT,
    CMU_UI_DEBUG,
};

enum CMU_ACTION
{
    CMU_ACTION_WLINK_GET_DESCTIPTOR,
    CMU_ACTION_WLINK_LOAD_FW,
    CMU_ACTION_WLINK_LOAD_SBL,
    CMU_ACTION_WLINK_LOAD_TSET_LANGUAGE,
    CMU_ACTION_WLINK_LOAD_TSET_REVOCATION,
    CMU_ACTION_WLINK_LOAD_TSET_EXCEPTION,
    CMU_ACTION_WLINK_LOAD_TSET_CA_KEYS,
    CMU_ACTION_WLINK_LOAD_TSET_BIN,
    CMU_ACTION_WLINK_LOAD_TSET_PW,
    CMU_ACTION_WLINK_LOAD_TSET_MCL,
    CMU_ACTION_WLINK_LOAD_TSET_AMX,
    CMU_ACTION_WLINK_LOAD_TSET_DPASS,
    CMU_ACTION_WLINK_LOAD_TSET_JCB,
    CMU_ACTION_WLINK_READ_RSA_SIGN_MANUF,
    CMU_ACTION_WLINK_READ_RSA_ENC_TERM,
    CMU_ACTION_WLINK_READ_RSA_SIGN_TERM,
    CMU_ACTION_WLINK_START_KEY_INJECTION,
    CMU_ACTION_WLINK_READ_SERIAL_NUMBER,
    CMU_ACTION_WLINK_WRITE_RSA_SIGN_SERVER,
    CMU_ACTION_WLINK_WRITE_SESSION,
    CMU_ACTION_WLINK_WRITE_SESSION_SIGN,
    CMU_ACTION_WLINK_WRITE_TR31_DUKPT,
    CMU_ACTION_WLINK_WRITE_TR31_HMAC_SALT,
    CMU_ACTION_WLINK_WRITE_KSN,
    CMU_ACTION_WLINK_WRITE_RSA_SIGN_CONFIG,
    CMU_ACTION_WLINK_END_KEY_INJECTION,
    CMU_ACTION_WLINK_SEND_RFR_COMMAND,

    CMU_ACTION_HID_MGMT_COMMAND = 0x40,
    CMU_ACTION_HID_LOAD_HEXFILE,

    CMU_ACTION_PCSC_TRANSCEIVE_APDU = 0x80
};

struct CMU_SETTINGS
{
    CMU_UI_MODE UI_Mode;
    CMU_ACTION Action;
    char RFR_type;
    char *tty_Device;                                      // CPM Wlink interface
    char *Update_File_Path;
    char *hidraw_Device;                                   // CPM management interface
    char hid_data[64];
    KEY_INJECTION_TYPE Key_Injection_Type;
    int extra_timeout_mS;
    char *PCSC_Reader_Name;
    char APDU_Data[256];
    int APDU_Size;
};

struct CMU_ERROR_STRING
{
    int ID;
    const char *String;
};

//-------------------------------------------------------------------------------------------------
// Function declerations
void Print_Banner(void);
void Print_Usage(void);
const char *Get_Error_String(int _errorCode);
void Print_Error(int _errorCode);
int Parse_CLI(int _argc, char *_argv[]);
int Parse_CLI_Hex_Data(char *_argv[], unsigned int _argc, char *_Data_Buffer, int _Size);
int Write_File(class WLINK *_Wlink, int _File_ID, int _extra_tout_mS);
int Read_File(class WLINK *_Wlink, int _File_ID, int _extra_tout_mS);
int Key_Injection(class WLINK *_Wlink, KEY_INJECTION_TYPE _key_type, bool _isStart, int _extra_tout_mS);

#endif