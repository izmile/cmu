#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Setup RX phase value
#
# > ./do_phase.sh <threshold value = 0 to 15> <hid device>
#
#--------------------------------------------------------------------------------------------------
PHASE_ARG="${1:-5}"
HID="${2:-/dev/hidraw0}"
EXIT_VAL=1
VERSION="0.3"
EXECUTABLE="cmu"

#--------------------------------------------------------------------------------------------------
if [[ $# == 0 ]]; then
   echo "Set RX phase v${VERSION}"
   echo "Usage:"
   echo "do_phase.sh <phase value = 0 to 15> <hid device (default = /dev/hidraw0)>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $PHASE_ARG == "0" ]] || [[ $PHASE_ARG == "00" ]]; then
   PHASE="00"
elif [[ $PHASE_ARG == "1" ]] || [[ $PHASE_ARG == "01" ]]; then
   PHASE="01"
elif [[ $PHASE_ARG == "2" ]] || [[ $PHASE_ARG == "02" ]]; then
   PHASE="02"
elif [[ $PHASE_ARG == "3" ]] || [[ $PHASE_ARG == "03" ]]; then
   PHASE="03"
elif [[ $PHASE_ARG == "4" ]] || [[ $PHASE_ARG == "04" ]]; then
   PHASE="04"
elif [[ $PHASE_ARG == "5" ]] || [[ $PHASE_ARG == "05" ]]; then
   PHASE="05"
elif [[ $PHASE_ARG == "6" ]] || [[ $PHASE_ARG == "06" ]]; then
   PHASE="06"
elif [[ $PHASE_ARG == "7" ]] || [[ $PHASE_ARG == "07" ]]; then
   PHASE="07"
elif [[ $PHASE_ARG == "8" ]] || [[ $PHASE_ARG == "08" ]]; then
   PHASE="08"
elif [[ $PHASE_ARG == "9" ]] || [[ $PHASE_ARG == "09" ]]; then
   PHASE="09"
elif [[ $PHASE_ARG == "10" ]]; then
   PHASE="0A"
elif [[ $PHASE_ARG == "11" ]]; then
   PHASE="0B"
elif [[ $PHASE_ARG == "12" ]]; then
   PHASE="0C"
elif [[ $PHASE_ARG == "13" ]]; then
   PHASE="0D"
elif [[ $PHASE_ARG == "14" ]]; then
   PHASE="0E"
elif [[ $PHASE_ARG == "15" ]]; then
   PHASE="0F"
else
   echo "ERROR: RX phase value shall be 0 to 15"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -hid $HID -hdata 06 00 08 00 00 $PHASE
if [[ $? != 0 ]]; then
   echo "ERROR: Failed to set RX phase = "$PHASE
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "RX phase value updated"
exit 0
#--------------------------------------------------------------------------------------------------
