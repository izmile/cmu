#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Setup RX threshold value
#
# > ./do_threshold.sh <threshold value = 0 to 15> <type> <hid device>
#
#--------------------------------------------------------------------------------------------------
THRESHOLD_ARG="${1:-5}"
TYPE_ARG="${2:-AB}"
HID="${3:-/dev/hidraw0}"
EXIT_VAL=1
VERSION="0.3"

#--------------------------------------------------------------------------------------------------
if [[ $# == 0 ]]; then
   echo "Set RX threshold v${VERSION}"
   echo "Usage:"
   echo "do_threshold.sh <threshold value = 0 to 15> <type (default = AB)> <hid device (default = /dev/hidraw0)>"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
# Check architecture
((EXIT_VAL=EXIT_VAL+1))
ARCH=`uname -m`
case ${ARCH:0:3} in
    x86)
        EXECUTABLE="cmu"
    ;;
    arm)
        EXECUTABLE="cmuarm"
    ;;
    *)
        echo "ERROR: unsupported architecture!"
        exit $EXIT_VAL
    ;;
esac

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $TYPE_ARG == "A" ]]; then
   TYPE="01"
elif [[ $TYPE_ARG == "B" ]]; then
   TYPE="02"
elif [[ $TYPE_ARG == "AB" ]]; then
   TYPE="00"
else
   echo "ERROR: <type> argument shall be A, B or AB"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
if [[ $THRESHOLD_ARG == "0" ]] || [[ $THRESHOLD_ARG == "00" ]]; then
   THRESHOLD="00"
elif [[ $THRESHOLD_ARG == "1" ]] || [[ $THRESHOLD_ARG == "01" ]]; then
   THRESHOLD="01"
elif [[ $THRESHOLD_ARG == "2" ]] || [[ $THRESHOLD_ARG == "02" ]]; then
   THRESHOLD="02"
elif [[ $THRESHOLD_ARG == "3" ]] || [[ $THRESHOLD_ARG == "03" ]]; then
   THRESHOLD="03"
elif [[ $THRESHOLD_ARG == "4" ]] || [[ $THRESHOLD_ARG == "04" ]]; then
   THRESHOLD="04"
elif [[ $THRESHOLD_ARG == "5" ]] || [[ $THRESHOLD_ARG == "05" ]]; then
   THRESHOLD="05"
elif [[ $THRESHOLD_ARG == "6" ]] || [[ $THRESHOLD_ARG == "06" ]]; then
   THRESHOLD="06"
elif [[ $THRESHOLD_ARG == "7" ]] || [[ $THRESHOLD_ARG == "07" ]]; then
   THRESHOLD="07"
elif [[ $THRESHOLD_ARG == "8" ]] || [[ $THRESHOLD_ARG == "08" ]]; then
   THRESHOLD="08"
elif [[ $THRESHOLD_ARG == "9" ]] || [[ $THRESHOLD_ARG == "09" ]]; then
   THRESHOLD="09"
elif [[ $THRESHOLD_ARG == "10" ]]; then
   THRESHOLD="0A"
elif [[ $THRESHOLD_ARG == "11" ]]; then
   THRESHOLD="0B"
elif [[ $THRESHOLD_ARG == "12" ]]; then
   THRESHOLD="0C"
elif [[ $THRESHOLD_ARG == "13" ]]; then
   THRESHOLD="0D"
elif [[ $THRESHOLD_ARG == "14" ]]; then
   THRESHOLD="0E"
elif [[ $THRESHOLD_ARG == "15" ]]; then
   THRESHOLD="0F"
else
   echo "ERROR: RX threshold value shall be 0 to 15"
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
((EXIT_VAL=EXIT_VAL+1))
./$EXECUTABLE -hid $HID -hdata 06 00 07 00 $TYPE 00 $THRESHOLD
if [[ $? != 0 ]]; then
   echo "ERROR: Failed to set threshold = "$THRESHOLD
   exit $EXIT_VAL
fi

#--------------------------------------------------------------------------------------------------
echo "RX threshold value updated"
exit 0
#--------------------------------------------------------------------------------------------------

