//-------------------------------------------------------------------------------------------------
// This file has the function declerations for common functions, definitions, macros
// Author : Mohamed Ismail Bari
// Compiler : gcc/g++
// Date : 01-Jun-2020
//-------------------------------------------------------------------------------------------------
#ifndef _COMMON_H_
#define _COMMON_H_

#define RED                            "\x1B[31m"
#define GREEN                          "\x1B[32m"
#define YELLOW                         "\x1B[33m"
#define BLUE                           "\x1B[34m"
#define MAGENTA                        "\x1B[35m"
#define CYNAN                          "\x1B[36m"
#define WHTITE                         "\x1B[37m"
#define RESET                          "\x1B[0m"

//------------------------------------------------------------------------------------------------
// Macros
//------------------------------------------------------------------------------------------------
#define uint_32_byte0(x)                                   ((uint8_t)x & 0xFF)
#define uint_32_byte1(x)                                   ((uint8_t)(x >> 8) & 0xFF)
#define uint_32_byte2(x)                                   ((uint8_t)(x >> 16) & 0xFF )
#define uint_32_byte3(x)                                   ((uint8_t)(x >> 24) & 0xFF)

#define uint_16_byte0(x)                                   ((uint8_t)x & 0xFF)
#define uint_16_byte1(x)                                   ((uint8_t)(x >> 8) & 0xFF)

#define uint_16_Toggle_Endian(x)                           (uint16_t)( (((uint16_t)(x)&0xFF00)>>8) | (((uint16_t)(x)&0x00FF)<<8) )
#define uint_24_Toggle_Endian(x)                           (uint32_t)( (((uint32_t)(x)&0x00FF0000)>>16) | ((uint32_t)(x)&0x0000FF00) | (((uint32_t)(x)&0x000000FF)<<16) )
#define uint_32_Toggle_Endian(x)                           (uint32_t)( (((uint32_t)(x)&0xFF000000)>>24) | (((uint32_t)(x)&0x00FF0000)>>8) | (((uint32_t)(x)&0x0000FF00)<<8) | (((uint32_t)(x)&0x000000FF)<<24) )

#define isChar_Number(_char)                               (_char >= '0' && _char <= '9')
#define isChar_Uppercase(_char)                            (_char >= 'A' && _char <= 'Z')
#define isChar_Lowercase(_char)                            (_char >= 'a' && _char <= 'z')
#define isChar_Alphabet(_char)                             (isChar_Uppercase(_char) || isChar_Lowercase(_char))
#define isChar_AlphaNumeric(_char)                         (isChar_Alphabet(_char) || isChar_Number(_char))
#define isChar_Space(_char)                                (_char == 0x20)
#define isChar_Printable(_char)                            (_char >= 0x20 && _char <= 0x7E)
#define isChar_NULL(_char)                                 (_char == 0)

#endif